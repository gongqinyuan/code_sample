<?php
$manifest = array(
    'author' => 'Hyundai NZ',
    'name' => 'HNZ: WP1012 - Facebook Attribution',
    'description' => '',
    'published_date' => '2020-02-03',
    'version' => '001',
    'acceptable_sugar_flavors' => array('PRO','CORP','ENT','ULT'),
    'acceptable_sugar_versions' => array(
        'exact_matches' => array(),
        'regex_matches' => array('9\\.(.*?)\\.(.*?)'),
    ),
    'icon' => '',
    'type' => 'module',
    'is_uninstallable' => true,
);

$installdefs = array(
    'id' => 'scrmFacebookAttribution',
    'post_execute' => array(
        '<basepath>/scripts/post_execute.php',
    ),
    'post_uninstall' => array(
        '<basepath>/scripts/post_uninstall.php',
    ),
    'copy' => array(
        array(
            'from' => '<basepath>/custom',
            'to' => 'custom',
        ),
    ),
);