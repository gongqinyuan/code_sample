<?php

$job_strings[] = 'wp1012FacebookAttribution_Scheduled_Job';

use scrm\Model\facebookAttributionsJob;

function wp1012FacebookAttribution_Scheduled_Job()
{
    $wp_id = 'wp1012FacebookAttribution';
    $GLOBALS['log']->fatal($wp_id . " START");
    $dc = new facebookAttributionsJob();
    $dc->run();
    $GLOBALS['log']->fatal($wp_id . " DONE");
    return true;
}



