<?php

namespace scrm\scrmModuleManagers;

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

use BeanFactory;
use cloudtech\ctutils\v0013\CtBean;
use cloudtech\ctutils\v0013\CtUtils;
use SugarQuery;

/**
 * Class FacebookAttributions
 * @package scrm\scrmModuleManagers
 */
class FacebookAttributions
{


    /**
     * @var string
     */
    private $name_prefix = 'FBA';

    private $module_name = '';

    /**
     * @param \SugarBean $bean
     * @param $event
     * @param array $arguments
     */
    public function main(\SugarBean $bean, $event, array $arguments)
    {
        $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - ' . __METHOD__);
        $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - $bean-id =  ' . $bean->id);
        $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Module = ' . $bean->module_name);
        $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - export_fba_c = ' . $bean->export_fba_c);
        $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - type = ' . $bean->type);
        $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - sale_type_c = ' . $bean->sale_type_c);

        $this->module_name = $bean->module_name;
        $beanFBA_Facebook_Attributions = null;

        $event_name = 'Default';
        $accountBean = null;

        if(!$bean->export_fba_c) {
            $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - '.$bean->module_name.' Early Return.');
            return true;
        }

        if ($this->module_name == 'Cases') {
            $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution ---------------------------'.$this->module_name.'-----------------------------------');
            if (($bean->type != 'Profile') || ($bean->sale_type_c == 'Dealer Demo' || $bean->sale_type_c == 'Internal Demo')) {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Cases Early Return.');
                return true;
            }
            $event_name = 'Purchase';

            $link = 'accounts';
            $parentBean = false;
            if ($bean->load_relationship($link)) {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Loaded $link');
                $relatedBeans = $bean->$link->getBeans();
                if (!empty($relatedBeans)) {
                    reset($relatedBeans);
                    $parentBean = current($relatedBeans);
                }
            } else {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Failed to load $link');
            }

            if ($parentBean) {
                $accountBean = $parentBean;
            }

            // dl_dealerships_opportunities_1_name
            $link = 'dl_dealerships_cases_1';
            $parentBean = false;
            if ($bean->load_relationship($link)) {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Loaded $link');
                $relatedBeans = $bean->$link->getBeans();
                if (!empty($relatedBeans)) {
                    reset($relatedBeans);
                    $parentBean = current($relatedBeans);
                }
            } else {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Failed to load $link');
            }
            $dealerBean = null;
            if ($parentBean) {
                $dealerBean = $parentBean;
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - $dealerBean->name =  ' . $dealerBean->name);
            }

            $link = 'opportunities_cases_1';
            $parentBean = false;
            if ($bean->load_relationship($link)) {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Loaded $link');
                $relatedBeans = $bean->$link->getBeans();
                if (!empty($relatedBeans)) {
                    reset($relatedBeans);
                    $parentBean = current($relatedBeans);
                }
            } else {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Failed to load $link');
            }
            $opportunityBean = null;
            if ($parentBean) {
                $opportunityBean = $parentBean;
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - $opportunityBean->name =  ' . $opportunityBean->name);
            }

            if ($opportunityBean) {
                $link = 'mod_models_opportunities_1';
                $parentBean = false;
                if ($opportunityBean->load_relationship($link)) {
                    $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Loaded $link');
                    $relatedBeans = $opportunityBean->$link->getBeans();
                    if (!empty($relatedBeans)) {
                        reset($relatedBeans);
                        $parentBean = current($relatedBeans);
                    }
                } else {
                    $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Failed to load $link: '.$link);
                }
                $modelBean = null;
                if ($parentBean) {
                    $modelBean = $parentBean;
                    $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - $modelBean->name =  ' . $modelBean->name);
                }
            }
        } else if ($this->module_name == 'Opportunities') {
            $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution ---------------------------'.$this->module_name.'-----------------------------------');
            $event_name = 'CompleteRegistration';

            $link = 'mod_models_opportunities_1';
            $parentBean = false;
            if ($bean->load_relationship($link)) {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Loaded $link');
                $relatedBeans = $bean->$link->getBeans();
                if (!empty($relatedBeans)) {
                    reset($relatedBeans);
                    $parentBean = current($relatedBeans);
                }
            } else {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Failed to load $link: '.$link);
            }
            $modelBean = null;
            if ($parentBean) {
                $modelBean = $parentBean;
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - $modelBean->name =  ' . $modelBean->name);
            }


            $link = 'accounts';
            $parentBean = false;
            if ($bean->load_relationship($link)) {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Loaded $link');
                $relatedBeans = $bean->$link->getBeans();
                if (!empty($relatedBeans)) {
                    reset($relatedBeans);
                    $parentBean = current($relatedBeans);
                }
            } else {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Failed to load $link: '.$link);
            }

            if ($parentBean) {
                $accountBean = $parentBean;
            }


            $link = 'dl_dealerships_opportunities_1'; // dl_dealerships_opportunities_1_name
            $parentBean = false;
            if ($bean->load_relationship($link)) {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Loaded $link');
                $relatedBeans = $bean->$link->getBeans();
                if (!empty($relatedBeans)) {
                    reset($relatedBeans);
                    $parentBean = current($relatedBeans);
                }
            } else {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Failed to load $link: '.$link);
            }
            $dealerBean = null;
            if ($parentBean) {
                $dealerBean = $parentBean;
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - $dealerBean->name =  ' . $dealerBean->name);
            }

        } else if ($this->module_name == 'TDR_Test_Drives') {
            $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution ---------------------------'.$this->module_name.'-----------------------------------');

            $event_name = 'CompleteRegistration';

            $link = 'opportunities_tdr_test_drives_1';
            $parentBean = false;
            if ($bean->load_relationship($link)) {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Loaded $link');
                $relatedBeans = $bean->$link->getBeans();
                if (!empty($relatedBeans)) {
                    reset($relatedBeans);
                    $parentBean = current($relatedBeans);
                }
            } else {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Failed to load $link: '.$link);
            }
            $opportunityBean = null;
            if ($parentBean) {
                $opportunityBean = $parentBean;
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - $opportunityBean->name =  ' . $opportunityBean->name);
            }

            if ($opportunityBean) {
                $link = 'mod_models_opportunities_1';
                $parentBean = false;
                if ($opportunityBean->load_relationship($link)) {
                    $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Loaded $link');
                    $relatedBeans = $opportunityBean->$link->getBeans();
                    if (!empty($relatedBeans)) {
                        reset($relatedBeans);
                        $parentBean = current($relatedBeans);
                    }
                } else {
                    $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Failed to load $link: '.$link);
                }
                $modelBean = null;
                if ($parentBean) {
                    $modelBean = $parentBean;
                    $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - $modelBean->name =  ' . $modelBean->name);
                }
            }

            $link = 'accounts';
            $parentBean = false;
            if ($opportunityBean->load_relationship($link)) {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Loaded $link');
                $relatedBeans = $opportunityBean->$link->getBeans();
                if (!empty($relatedBeans)) {
                    reset($relatedBeans);
                    $parentBean = current($relatedBeans);
                }
            } else {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Failed to load $link: '.$link);
            }

            if ($parentBean) {
                $accountBean = $parentBean;
            }


            $link = 'dl_dealerships_opportunities_1'; // dl_dealerships_opportunities_1_name
            $parentBean = false;
            if ($opportunityBean->load_relationship($link)) {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Loaded $link');
                $relatedBeans = $opportunityBean->$link->getBeans();
                if (!empty($relatedBeans)) {
                    reset($relatedBeans);
                    $parentBean = current($relatedBeans);
                }
            } else {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Failed to load $link: '.$link);
            }
            $dealerBean = null;
            if ($parentBean) {
                $dealerBean = $parentBean;
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - $dealerBean->name =  ' . $dealerBean->name);
            }

        } else {
            $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Defaulted Early Return.');
            return true;
        }

        $query = new SugarQuery();
        $query->select('id');
        $query->from(BeanFactory::getBean('FBA_Facebook_Attributions'));
        $query->where()->equals('source_id', $bean->id);
        $rs = $query->execute();
        $preparedStmt = $query->compile();
        $sql = $preparedStmt->getSQL();
        $parameters = $preparedStmt->getParameters();
        $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - $sql =  ' . $sql);
        $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - $parameters =  ' . print_r($parameters, true));

        if (count($rs) == 1) {
            $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - Found Input Buffer... Updating record');
            $ctBeanResult = CtBean::getByField('FBA_Facebook_Attributions', 'source_id', $bean->id, false);
            $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution PR - ' . print_r($ctBeanResult, true));
            if (count($ctBeanResult) == 1) {
                $beanFBA_Facebook_Attributions = BeanFactory::getBean('FBA_Facebook_Attributions', $ctBeanResult);
                if ($beanFBA_Facebook_Attributions) {
                    $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - $beanFBA_Facebook_Attributions->id ===  ' . $beanFBA_Facebook_Attributions->id);
                }
            }
        } else {
            $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution -Could not find Input Buffer');
            $beanFBA_Facebook_Attributions = BeanFactory::newBean('FBA_Facebook_Attributions');
            $beanFBA_Facebook_Attributions->source_id = $bean->id;
            $beanFBA_Facebook_Attributions->status = 'Pending';
        }

        if ($beanFBA_Facebook_Attributions) {
            $beanFBA_Facebook_Attributions->event_time = $bean->date_entered;
            $beanFBA_Facebook_Attributions->team_id = $bean->team_id;
            $beanFBA_Facebook_Attributions->team_set_id = $bean->team_set_id;
            $beanFBA_Facebook_Attributions->currency = 'NZD';

            if ($accountBean) {
                $beanFBA_Facebook_Attributions->fn = $accountBean->ctacf_first_name;
                $beanFBA_Facebook_Attributions->ln = $accountBean->ctacf_last_name;
                $phone = null;
                if (!empty(trim($accountBean->ctacf_phone_mobile))) {
                    $phone = $accountBean->ctacf_phone_mobile;
                } else if (!empty(trim($accountBean->phone_office))) {
                    $phone = $accountBean->phone_office;
                } else if (!empty(trim($accountBean->phone_home_c))) {
                    $phone = $accountBean->phone_home_c;
                }
                if ($phone) {
                    $beanFBA_Facebook_Attributions->phone = $phone;
                }
                $primaryEmailAddress = $accountBean->emailAddress->getPrimaryAddress($accountBean);
                if ($primaryEmailAddress) {
                    $beanFBA_Facebook_Attributions->email = $primaryEmailAddress;
                }
            }

            if ($modelBean) {

                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Setting Model details ==================================================================');
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - $modelBean->name =  ' . $modelBean->name);
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - $modelBean->brochure_price_c =  ' . $modelBean->brochure_price_c);

                $beanFBA_Facebook_Attributions->model = $modelBean->name;
                $beanFBA_Facebook_Attributions->event_value = $modelBean->brochure_price_c;

                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - $beanFBA_Facebook_Attributions->name =  ' . $beanFBA_Facebook_Attributions->model);
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - $beanFBA_Facebook_Attributions->brochure_price_c =  ' . $beanFBA_Facebook_Attributions->event_value);

            } else {
                $GLOBALS['log']->fatal('LOG: wp1012FacebookAttribution - '.$this->module_name.' - Failed setting Model details');
            }

            if ($dealerBean) {
                $beanFBA_Facebook_Attributions->dealer = $dealerBean->name;
            }

            if ($this->module_name == 'Cases') {
                $beanFBA_Facebook_Attributions->name = $this->name_prefix . ' - ' . $accountBean->name . ' - ' . $bean->source_vin_c;
            } else if ($this->module_name == 'Opportunities') {
                $beanFBA_Facebook_Attributions->name = $this->name_prefix . ' - ' . $accountBean->name . ' - ' . $modelBean->name;
            } else if ($this->module_name == 'TDR_Test_Drives') {
                $beanFBA_Facebook_Attributions->name = $this->name_prefix . ' - ' . $accountBean->name . ' - ' . $modelBean->name;
            }

//            $beanFBA_Facebook_Attributions->model = 'test2';
//            $beanFBA_Facebook_Attributions->event_value = 111;

            $beanFBA_Facebook_Attributions->event_name = $event_name;
            $beanFBA_Facebook_Attributions->save();
        }
    }
}
