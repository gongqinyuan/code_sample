<?php

namespace scrm\Model;

use cloudtech\ctutils\v0013\CtBean;

class facebookAttributionsJob
{

    /**
     * @var string
     */
    var $log_prefix = 'wp1012FacebookAttribution';

    /**
     * @var array
     */
    var $teams_and_tokens = array();

    /**
     * @var array
     */
    var $access_token = null;

    /**
     * @var integer
     */
    var $wait_before_push;

    /**
     * @var integer
     */
    var $scrm_gmt_offset;

    var $request_data = null;

    /**
     * @throws \cloudtech\ctutils\v0013\CtException
     */
    public function run()
    {
        $GLOBALS['log']->fatal($this->log_prefix . " Running...... |");

        // TODO: look at possible use of SugarQuery here:
        // Reference: https://support.sugarcrm.com/Documentation/Sugar_Developer/Sugar_Developer_Guide_9.1/Data_Framework/Database/SugarQuery/SugarQuery_Conditions/#dateRange

        $fba_id_list = CtBean::getByField('FBA_Facebook_Attributions', 'status', 'Pending');
        $GLOBALS['log']->fatal($this->log_prefix . ' $fba_id_list = ' . print_r($fba_id_list, true));

        global $sugar_config;
        $this->access_token = $sugar_config['fba_settings']['access_token'];
        $this->teams_and_tokens = $sugar_config['fba_settings']['teams_and_token'];
        $this->wait_before_push = isset($sugar_config['fba_settings']['wait_before_push']) ? $sugar_config['fba_settings']['wait_before_push']  : 0;
        $this->scrm_gmt_offset = isset($sugar_config['scrm_gmt_offset']) ? $sugar_config['scrm_gmt_offset'] : 0;

        if ($fba_id_list) {
            if (is_array($fba_id_list)) {
                foreach ($fba_id_list as $fba_id) {
                    $this->postFacebookEventRequest($fba_id);
                }
            } else {
                $this->postFacebookEventRequest($fba_id_list);
            }
        }
    }

    /**
     * @param $fba_id
     * @return bool
     */
    private function postFacebookEventRequest($fba_id)
    {
        $GLOBALS['log']->fatal($this->log_prefix . ' $fba_id = ' . $fba_id);

        $beanFBA_Facebook_Attributions = \BeanFactory::getBean('FBA_Facebook_Attributions', $fba_id);

        $unix_ts_now = time();

        $date_entered1 = $beanFBA_Facebook_Attributions->date_entered;

        $date_entered_unix_ts = strtotime($date_entered1);

        $unix_ts_diff = $unix_ts_now - ($date_entered_unix_ts + $this->scrm_gmt_offset);

        if($unix_ts_diff < $this->wait_before_push) {
            $GLOBALS['log']->fatal($this->log_prefix . ' $beanFBA_Facebook_Attributions date_entered is still within hold period of ' . $this->wait_before_push. ' seconds.');
            $GLOBALS['log']->fatal($this->log_prefix . ' $beanFBA_Facebook_Attributions skipping for now : ' . $beanFBA_Facebook_Attributions->id);
            return true;
        }

        if (isset($this->teams_and_tokens[$beanFBA_Facebook_Attributions->team_id])) {
            $GLOBALS['log']->fatal($this->log_prefix . " processResponse()...... | Token Check OK!");
            $even_set_id = $this->teams_and_tokens[$beanFBA_Facebook_Attributions->team_id]['event_set_id'];
        } else {
            $GLOBALS['log']->fatal($this->log_prefix . " processResponse()...... | Token Check Fail!");
            return true;
        }

        $GLOBALS['log']->fatal($this->log_prefix . " \$data_list: \$event_value = ".$beanFBA_Facebook_Attributions->event_value);
        $new_event_value = $beanFBA_Facebook_Attributions->event_value;
        if($beanFBA_Facebook_Attributions->event_value == 0) {
            $new_event_value = '50000';
        }

        if($beanFBA_Facebook_Attributions->event_name == 'CompleteRegistration') {
            $new_event_value = '0';
        }

        $GLOBALS['log']->fatal($this->log_prefix . " \$data_list: \$new_event_value = ".$new_event_value);

        $match_keys_list = array();
        if(trim($beanFBA_Facebook_Attributions->fn) != '') {
            $match_keys_list['fn'] = hash('sha256', $beanFBA_Facebook_Attributions->fn);
        }
        if(trim($beanFBA_Facebook_Attributions->ln) != '') {
            $match_keys_list['ln'] = hash('sha256', $beanFBA_Facebook_Attributions->ln);
        }
        if(trim($beanFBA_Facebook_Attributions->phone) != '') {
            $match_keys_list['phone'] = hash('sha256', $beanFBA_Facebook_Attributions->phone);
        }
        if(trim($beanFBA_Facebook_Attributions->email) != '') {
            $match_keys_list['email'] = hash('sha256', $beanFBA_Facebook_Attributions->email);
        }


        $data_list = array(
            'match_keys' => $match_keys_list,
            'event_time' => strtotime($beanFBA_Facebook_Attributions->event_time),
            'event_name' => $beanFBA_Facebook_Attributions->event_name,
            'currency' => 'NZD',
            'value' => $new_event_value,
            'custom_data' => array(
                'model' => $beanFBA_Facebook_Attributions->model,
                'dealer' => $beanFBA_Facebook_Attributions->dealer
            )
        );

//        $command_string = 'curl -F \'access_token=' . $this->access_token . '\' -F \'upload_tag=store_data\' -F \'data=[' . json_encode($data_list) . ']\'  https://graph.facebook.com/v5.0/'.$even_set_id.'/events';
//        $GLOBALS['log']->fatal($this->log_prefix .' - '.$command_string);


        $temp_data = json_encode($data_list);
        $temp_data = addcslashes($temp_data, '"\\/');

//        exec('curl -F \'access_token=' . $this->access_token . '\' -F \'upload_tag=store_data\' -F \'data=[' . json_encode($data_list) . ']\'  https://graph.facebook.com/v5.0/'.$even_set_id.'/events'
//            , $response);

        exec('curl -F \'access_token=' . $this->access_token . '\' -F \'upload_tag=store_data\' -F "data=[' . $temp_data . ']"  https://graph.facebook.com/v5.0/'.$even_set_id.'/events'
            , $response);


        $GLOBALS['log']->fatal($this->log_prefix .' - '.print_r(json_encode($data_list), true));
        $GLOBALS['log']->fatal($this->log_prefix .' - '.print_r($response, true));
        $GLOBALS['log']->fatal($this->log_prefix .' - '.print_r(json_decode($response, true)));

        $this->request_data = json_encode($data_list);
        $this->processResponse($response, $beanFBA_Facebook_Attributions);
        return true;

        /**
         * curl \
         * -F 'access_token=' \
         * -F 'upload_tag=store_data' \
         * -F 'data=[{"match_keys":{"fn": [""], "ln": [""], "phone": [""], "email": [""]},"event_time":"1456870902","event_name":"Purchase","currency":"NZD","value":"50000","custom_data":{"model":"Koleos","dealer":"Auckland"}}]' \
         * https://graph.facebook.com/v5.0//events
         */
    }

    /**
     * @param array $response
     */
    private function processResponse(array $response, \SugarBean $beanFBA_Facebook_Attributions)
    {
        $GLOBALS['log']->fatal($this->log_prefix . " processResponse() Start ");
        if (is_array($response)) {
            if (isset($response[0])) {
                $save = false;
                $response_row = json_decode($response[0]);
                if (isset($response_row->id) && isset($response_row->num_processed_entries)) {
                    $GLOBALS['log']->fatal($this->log_prefix . " processResponse() | Success!");
                    $beanFBA_Facebook_Attributions->status = 'Sent';
                    $save = true;
                } else {
                    $GLOBALS['log']->fatal($this->log_prefix . " processResponse() | Possible Error");
                    $beanFBA_Facebook_Attributions->status = 'Error';
                    $save = true;
                }

                if ($save) {
                    $GLOBALS['log']->fatal($this->log_prefix . " processResponse() | Saving \$beanFBA_Facebook_Attributions");
                    $now = gmdate('Y-m-d H:i:s');
                    $beanFBA_Facebook_Attributions->sent = $now;
                    $beanFBA_Facebook_Attributions->request_data = $this->request_data;
                    $beanFBA_Facebook_Attributions->response_data = $response[0];
                    $beanFBA_Facebook_Attributions->save();
                }
            } else {
                $GLOBALS['log']->fatal($this->log_prefix . " processResponse()...... | \$response[0] is not set");
            }
        } else {
            $GLOBALS['log']->fatal($this->log_prefix . " processResponse()...... | Response not an Array ");

        }
    }

    /**
     * Curl send post request, support HTTPS protocol
     * @param string $url The request url
     * @param array $data The post data
     * @param string $refer The request refer
     * @param int $timeout The timeout seconds
     * @param array $header The other request header
     * @return mixed
     */
    private function postRequest($url, $data, $refer = "", $timeout = 10, $header = [])
    {
        $curlObj = curl_init();
        $ssl = stripos($url, 'https://') === 0 ? true : false;
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_AUTOREFERER => 1,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)',
            CURLOPT_TIMEOUT => $timeout,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_0,
            CURLOPT_HTTPHEADER => ['Expect:'],
            CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
            CURLOPT_REFERER => $refer
        ];
        if (!empty($header)) {
            $options[CURLOPT_HTTPHEADER] = $header;
        }
        if ($refer) {
            $options[CURLOPT_REFERER] = $refer;
        }
        if ($ssl) {
            //support https
            $options[CURLOPT_SSL_VERIFYHOST] = false;
            $options[CURLOPT_SSL_VERIFYPEER] = false;
        }
        curl_setopt_array($curlObj, $options);
        $returnData = curl_exec($curlObj);
        if (curl_errno($curlObj)) {
            //error message
            $returnData = curl_error($curlObj);
        }
        curl_close($curlObj);
        return $returnData;
    }


}