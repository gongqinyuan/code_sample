# Description
A Custom Web Client for Dialogflow V2 APIs, based on Dialogflow Nodejs and Vuejs.

# Note
Please add Google Credentials JSON file at root level and update .env file.

# Config