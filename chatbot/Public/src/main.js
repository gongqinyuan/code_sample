import Vue from 'vue';
import VueCarousel from 'vue-carousel';
import App from './App.vue';

Vue.config.devtools = true;

Vue.use(VueCarousel);

new Vue({
    el: '#chatbot-app',
    render: h => h(App)
}).$mount('#chatbot-app');