export default {
    app: {
        muted: false, // <- mute microphone by default
        googleIt: true // <- ask users to google their request, in case of input.unknown action
    },
    locale: {
        strings: {
            welcomeTitleNZ: "",
            welcomeDescriptionNZ: "",
            welcomeTitleOutsideNZ: "",
            welcomeDescriptionOutsideNZ: "",
            offlineTitle: "",
            offlineDescription: "",
            queryTitle: "Type your message here",
            voiceTitle: "Go ahead, I'm listening..."
        },
        settings: {
            speechLang: "en-GB", // <- output language
            recognitionLang: "en-US" // <- input(recognition) language
        }
    }
};