<?php

return [
	"AWS_KEY" => env('AWS_ACCESS_KEY_ID'),
	"AWS_SECRET" => env('AWS_SECRET_ACCESS_KEY'),
	"AWS_REGION" => env('AWS_DEFAULT_REGION'),
	"AWS_BUCKET" => env('AWS_BUCKET'),
	"API_KEY" => env('TINIFY_API_KEY')
];
