<?php

/*
 * Storage parameters
 *
 */

return [
	"prefix" => "DATA_",
	"postfix_history" => "_History",
	"postfix_published" => "_Published",
];