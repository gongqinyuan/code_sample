<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
	<link href="{{ asset('css/admin.css?r='.config('cache.cache_id')) }}" rel="stylesheet">
	<link rel="shortcut icon" type="image/x-icon" href="/src/img/icons/favicon.ico">
</head>
<body>
	<div class="container" id="login">
	<div class="admin-login">
		<div class="header">
			<a href="/admin/" class="router-link-active"><div class="logo bannerbot">BannerBot</div></a>
			<div class="menu">
				
			</div>
			<div class="logo none"></div>
		</div>
		<div class="builder">
			<div class="app">
				<div class="row">
					<div class="col-md-6 col-md-offset-4">
					@yield('content')
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
		</div>
	</div>
	</div>
</body>
</html>
