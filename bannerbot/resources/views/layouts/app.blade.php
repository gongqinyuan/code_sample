<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
	<link href="{{ asset('css/admin.css?r='.config('cache.cache_id')) }}" rel="stylesheet">
	<link rel="shortcut icon" type="image/x-icon" href="/src/img/icons/favicon.ico">
</head>
<body>
	@guest
	<div class="container" id="login">
	@else
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	@csrf
	</form>
	<div class="container" id="admin">
	@endguest
	@yield('content')
	</div>
	<script>
		var USER_NAME = "{{ Auth::user()->email }}";
	</script>
</body>
</html>
