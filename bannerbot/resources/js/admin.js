import Vue from 'vue';
import VueRouter from 'vue-router';
import Clipboard from 'v-clipboard'
import AdminPage from './adminpage'

import toast from './libs/toast';
import qs from 'qs'
import axios from 'axios'
import bowser from 'bowser'

import Bobocrop from './libs/bobocrop';
import Boboshot from './libs/boboshot';

Vue.use(VueRouter);
Vue.use(Clipboard);
Vue.prototype.$toast = toast;
Vue.prototype.$axios = axios;
Vue.prototype.$qs = qs;
Vue.prototype.$browser = bowser.getParser(window.navigator.userAgent).getBrowserName();
Vue.prototype.$bcrop = Bobocrop;
Vue.prototype.$bshot = Boboshot;
Vue.prototype.$isset = function (v) { return (typeof v == 'undefined')?false:true; }

Vue.prototype.$authError = function (v) {
	// redirect to /admin/
	window.location.href='/admin/';
};

Vue.prototype.$aclCheck = function (r) {
		try {
			var tmp = JSON.parse(sessionStorage.getItem("acl"));
			if (tmp[r]) {
				return tmp[r];
			} else {
				return { r: false, w: false, x: false};
			}
		} catch (e) {
			return { r: false, w: false, x: false};
		}
	};

Vue.prototype.$processValue = function (type, value) {
		if (type == 'integer' || type == 'ad_id' || type == 'id') {
			if (value == null) {
				return 0;
			} else {
				return parseInt(value);
			}
		} else if (type == 'boolean' || type == 'active') {
			if (typeof value == 'boolean') {
				return value;
			} else if (value == null) {
				return false;
			} else {
				return (value && value.toLowerCase() == "true")?true:false;
			}
		} else if (type == 'array') {
			if (value == null) {
				return [];
			} else {
				return JSON.parse(JSON.stringify(value));
			}
		} else {
			// string, category, clickthrough_url, bannername
			if (value == null) {
				return '';
			} else {
				return (typeof value != 'undefined' && value != null)?value+"":'';
			}
		}
	};

Vue.prototype.$getBannerSettings = function (bannerSize) {
	return JSON.parse(sessionStorage.getItem("bannersSettings"))[bannerSize];
};
// bannerSize   string
// type         string|array
Vue.prototype.$getBannerSettingsByType = function (bannerSize, type) {
	var tmp = JSON.parse(sessionStorage.getItem("bannersSettings"))[bannerSize];
	var items = [];
	for (var i = 0; i < tmp.length; i++) {
		if (typeof tmp[i].type == 'string') {
			if (typeof type == 'string') {
				if (tmp[i].type == type) {
					items.push(tmp[i]);
				}
			} else if (typeof type == 'object') {
				if (type.indexOf(tmp[i].type) > -1) {
					items.push(tmp[i]);
				}
			}
		}
	}
	tmp = null;
	return items;
}

var loaded = {
		acl: false,
		cats: false,
		bannerSizes: false,
		overviewSettings: false,
		bannersSettings: false
	};

function initApp() {
	clearSettings();
	
	getAcl();
	getCats();
	getBannerSizes();
}

function clearSettings() {
	sessionStorage.removeItem("acl");
	sessionStorage.removeItem("overviewSettings");
	sessionStorage.removeItem("bannersSettings");
	sessionStorage.removeItem("cats");
	sessionStorage.removeItem("banners");
}

function getAcl() {
	axios.get("/api/acl")
	.then(response => {
		if (response.data.success == true) {
			// just for reference, real access check everytime on server side!
			sessionStorage.setItem("acl", JSON.stringify(response.data.message));
			loaded.acl = true;
			getSettings('overviewSettings');
			getSettings('bannersSettings');
		} else if (response.data.message.error.code == 401) {
			window.location.href='/admin/';
		} else {
			toast.error(response.data.message.error.message);
		}
	})
	.catch(e => {
		toast.error(e);
	})

}

function getSettings(type) {
	axios.get("/api/"+type)
	.then(response => {
		if (response.data.success == true) {
			sessionStorage.setItem(type, JSON.stringify(response.data.message.items));
		}
		loaded[type] = true;
		startApp();
	})
	.catch(e => {
		toast.error(e);
	})
}

function getCats () {
	axios.get("/api/category")
	.then(response => {
		if (response.data.success == true) {
			var tmp = response.data.message.items;
			var cats = { };
			for (var i = 0; i < tmp.length; i++) {
				tmp[i].deleted = false;
				tmp[i].vid = i;
				cats[tmp[i].ID] = tmp[i];
			}
			sessionStorage.setItem("cats", JSON.stringify(cats));
			loaded.bannerSizes = true;
			startApp();
		} else if (response.data.message.error.code == 401) {
			window.location.href='/admin/';
		} else {
			toast.error(response.data.message.error.message);
		}
	})
	.catch(e => {
		toast.error(e);
	})
}
function getBannerSizes () {
	axios.get("/api/bannersizes")
	.then(response => {
		if (response.data.success == true) {
			var tmp = response.data.message.items;
			var banners = {};
			for (var i = 0; i < tmp.length; i++) {
				tmp[i].deleted = false;
				tmp[i].vid = i;
				banners[tmp[i].ID] = tmp[i];
			}
			sessionStorage.setItem("banners", JSON.stringify(banners));
			loaded.cats = true;
			startApp();
		} else if (response.data.message.error.code == 401) {
			window.location.href='/admin/';
		} else {
			toast.error(response.data.message.error.message);
		}
	})
	.catch(e => {
		toast.error(e);
	})
}


function isLoaded() {
	for (var i in loaded) {
		if (loaded[i] != true) {
			return false;
		}
	}
	return true;
}

function startApp() {
	if (isLoaded()) {
		const init_route = Vue.prototype.$aclCheck('_init_');
		console.log("Run BannerBot", BANNERBOT_VERSION, "in", Vue.prototype.$browser, "with default route", init_route, bowser.parse(window.navigator.userAgent));
		var routes = [
				{ path: '*', redirect: init_route }
			];
			
			if (Vue.prototype.$aclCheck('/admin/items/').x === true) {
				console.log("Banners routes enabled");
				routes.push({ 
					path: "/admin/items/:category/:bannerSize/:bannerid/:bannerversion", 
					title: "Banners", 
					component: () => import('./admin/banners'), 
					props: (route) => ( {
						r: 'banners', 
						cat: route.params.category, 
						bannerSize: route.params.bannerSize, 
						bannerID: route.params.bannerid, 
						bannerVersion: route.params.bannerversion 
					}) 
				});
				routes.push({ 
					path: "/admin/items/:category/:bannerSize/:bannerid", 
					title: "Banners", 
					component: () => import('./admin/banners'), 
					props: (route) => ( {
						r: 'items', 
						cat: route.params.category, 
						bannerSize: route.params.bannerSize, 
						bannerID: route.params.bannerid
					}) 
				});
				routes.push({ 
					path: "/admin/items/:category/:bannerSize", 
					title: "Banners", 
					component: () => import('./admin/banners'), 
					props: (route) => ( {
						r: 'items', 
						cat: route.params.category, 
						bannerSize: route.params.bannerSize 
					}) 
				});
				routes.push({ 
					path: "/admin/items/:category", 
					title: "Banner Sizes", 
					component: () => import('./admin/bannersizes'), 
					props: (route) => ( {
						r: 'items', 
						cat: route.params.category 
					}) 
				});
				routes.push({ 
					path: "/admin/items", 
					component: () => import('./admin/items'), 
					props: { 
						r: 'items' 
					} 
				});
			}

			if (Vue.prototype.$aclCheck('/admin/overview/').x === true) {
				console.log("Overview routes enabled");
				routes.push({ 
					path: "/admin/overview/:bannerSize", 
					title: "Campaign overview", 
					component: () => import('./admin/overview'), 
					props: (route) => ( {
						r: 'overview', 
						bannerSize: route.params.bannerSize 
					}) 
				});
				routes.push({ 
					path: "/admin/overview/", 
					title: "Campaign overview", 
					component: () => import('./admin/overview'), 
					props: (route) => ( {
						r: 'overview' 
					}) 
				});
			}
			
			if (Vue.prototype.$aclCheck('_overviewbanner').x === true) {
				routes.push({ 
					path: "/admin/create/:category/:bannerSize/:bannerid", 
					title: "Banners", 
					component: () => import('./admin/banners'), 
					props: (route) => ( {
						r: 'create', 
						cat: route.params.category, 
						bannerSize: route.params.bannerSize, 
						bannerID: route.params.bannerid
					}) 
				});
				
				routes.push({ 
					path: "/admin/create/:category/:bannerSize", 
					title: "Banners", 
					component: () => import('./admin/banners'), 
					props: (route) => ( {
						r: 'create', 
						cat: route.params.category, 
						bannerSize: route.params.bannerSize 
					}) 
				});
				
				routes.push({ 
					path: "/admin/create/:category", 
					title: "Banner Sizes", 
					component: () => import('./admin/bannersizes'), 
					props: (route) => ( {
						r: 'create', 
						cat: route.params.category 
					}) 
				});

				routes.push({ 
					path: "/admin/create", 
					component: () => import('./admin/items'), 
					props: { 
						r: 'create' 
					} 
				});
			}

		const router = new VueRouter({ mode: 'history', routes: routes, scrollBehavior (to, from, savedPosition) {
				return { x: 0, y: 0 }
			} });


		const admin = new Vue({ 
				el: '#admin',
				router,
				render: h => h(AdminPage)
			});
	}
}

initApp();
startApp();
