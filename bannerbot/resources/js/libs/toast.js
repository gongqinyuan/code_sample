import iZtoast from 'izitoast'

const toast = {
    error: (message) => {
        return iZtoast.error({
            title: "Error",
            message: message,
            position: 'center',
            displayMode: 2
        });
    },
    warning: (message) => {
        return iZtoast.warning({
            title: "Warning",
            message: message,
            position: 'center',
            displayMode: 2
        });
    },
    success: (message) => {
        return iZtoast.success({
            title: "Success",
            message: message,
            position: 'center',
            timeout: 5000,
            displayMode: 2
        });
    }
};

export default toast;
