/*
	Boboshot - banner screenshot
*/
import Pica from 'pica';
import html2canvas from 'html2canvas';
window.html2canvas = html2canvas;

const Boboshot = function (obj, bobocrop_obj, config) {
	this.config = config;
	this.obj = false;
	this.obj_id = false;
	this.callback = false;
	this.bobocrop_obj = bobocrop_obj;


	// INIT component

	this.init = function (obj) {
		this.obj_id = obj+"";
		this.reInit();
	}
	
	this.reInit = function () {
		if (obj[0] == "#" && document.getElementById(obj.substring(1))) {
			this.obj = document.getElementById(obj.substring(1));
		} else if (obj[0] == "." && document.getElementsByClassName(obj.substring(1)).length>0) {
			this.obj = document.getElementsByClassName(obj.substring(1))[0];
		} else {
			this.obj = false;
			this.log("FATAL", "Unable to init object");
		}
		//this.obj = document.body;	// full html body for debug
	}


	// Screenshot of object
	this.screenshot = function (_callback) {
		this.reInit();
		this.callback = _callback;
		//this.bobocrop_obj.toBackground(function () { this.screenshot_step2() }.bind(this));
		this.screenshot_step2();
	}
	this.screenshot_step2 = function () {
		html2canvas(this.obj, { 
				scale: 4,
				logging: true,
				useCORS: false
			}).then(function(canvas) {
				var resize_canvas = document.createElement("canvas");
				var resize_context = resize_canvas.getContext("2d");
				resize_canvas.width = parseInt(canvas.width);
				resize_canvas.height = parseInt(canvas.height);
				// copy canvas to resize canvas
				resize_context.drawImage(canvas, 0, 0, canvas.width, canvas.height, 0,0, resize_canvas.width, resize_canvas.height);

				var resize_canvas_final = document.createElement("canvas");
				var resize_context_final = resize_canvas_final.getContext("2d");
				resize_canvas_final.width = parseInt(canvas.width)/4;
				resize_canvas_final.height = parseInt(canvas.height)/4;

				var pica = Pica();
				pica.resize(resize_canvas,resize_canvas_final, {
					unsharpAmount: 20,
					unsharpRadius: 0.51,
					unsharpThreshold: 20}).then(function (r) { 
						var ctx = resize_canvas_final.getContext("2d");
						ctx.globalAlpha = 1;
						ctx.rect(0, 0, resize_canvas_final.width, resize_canvas_final.height);
						ctx.lineWidth = 1;
						ctx.strokeStyle = "#000000";
						ctx.stroke();
						ctx.rect(0, 0, resize_canvas_final.width, resize_canvas_final.height);
						ctx.stroke();
						ctx.rect(0, 0, resize_canvas_final.width, resize_canvas_final.height);
						ctx.stroke();
						ctx.rect(0, 0, resize_canvas_final.width, resize_canvas_final.height);
						ctx.stroke();
						this.callback(resize_canvas_final); 
					}.bind(this));
		}.bind(this));		
	}
	
	// LOG handler
	
	this.log = function (s,m) {
		// TODO log levels by `s`
		console.log ("BoboShot:", s, m);
	}
	
	this.destroy = function () {
		// TODO destroy
	}
	
	this.init(obj);
}

export default Boboshot;
