/*
	Bobocrop - image crop
*/

import Pica from 'pica';
import './croppie/croppie';


const Bobocrop = function (obj, config, imageCallback, eventCallback) {
	this.config = config;
	this.obj = null;
	this.objViewport = null;
	this.objViewportFlag = false;
	this.objViewportTimeout = false;
	this.objViewportPica = Pica();
	this.callback = false;
	this.ready = false;
	this.c = null;
	this.u = null;
	this.imageCallback = imageCallback;
	this.eventCallback = eventCallback;
	this.b64_placeholder = false;
	this.mouseButton = false;
	this.sendMoveEvent = false;
	this.imageURL = false;
	this.imageSize = {w: 0, h:0};
	this.zoomLimitEnabled = false;
	this.cropLocked = false;


	// INIT component

	this.init = function (obj) {
		if (obj[0] == "#" && document.getElementById(obj.substring(1))) {
			this.obj = document.getElementById(obj.substring(1));
		} else if (obj[0] == "." && document.getElementsByClassName(obj.substring(1)).length>0) {
			this.obj = document.getElementsByClassName(obj.substring(1))[0];
		} else {
			this.obj = false;
			this.objViewport = false;
			this.log("FATAL", "Unable to init object "+obj);
		}
		if (this.obj !== false && this.obj !== null) {
			this.UploaderInit();
			this.initUI();
			this.initPlaceholder();
		}
	}
	
	this.isNewImage = function () {
		// new image inserted and Croppie initilized
		if (this.c !== null) {
			return true;
		}
		return false;
	}
		
	this.initUI = function () {
		//this.obj.style.width = this.config.size.w+"px";
		//this.obj.style.height = this.config.size.h+"px";
	}
	
	this.initPlaceholder = function () {
		if (typeof this.config.placeholder.src != 'undefined') {
			// use base64 encoded image to avoid CORS bug of html2canvas
			var img = new Image();
			img.crossOrigin = 'Anonymous';
			var that = this;
			img.onload = function() {
				var canvas = document.createElement('canvas');
				var ctx = canvas.getContext('2d');
				canvas.height = this.naturalHeight;
				canvas.width = this.naturalWidth;
				ctx.drawImage(this, 0, 0);
				that.b64_placeholder = canvas.toDataURL();
				that.obj.style.backgroundImage = "url('"+that.b64_placeholder+"')";
				if (that.imageCallback !== false) {
					that.imageCallback(that.b64_placeholder, 'init');
				}
			};
			img.src = this.config.placeholder.src+"?"+Math.random();	// add random for AWS
		}
	}
	this.clearPlaceholder = function () {
		if (this.obj) {
			this.obj.style.backgroundImage = "none";
		}
	}
	
	this.reinit = function () {
		// TODO destroy objects
		//  - initUI
		//  - Uploader
		//  - Croppie
		//  - Croppie update listener
	}
	
	this.destroy = function () {
		if (this.c !== null) {
			this.c.destroy();
			this.c = null;
		}
		this.clearEvents();
		this.UploaderDestroy();
		this.clearPlaceholder();
	}
	
	// create background object
	this.toBackground = function (_next) {
		this.crop(function (_next, canvas) {
			this.destroy();
			this.obj.className = "background";
			this.obj.style.backgroundImage = "url('"+canvas.toDataURL()+"')";
			_next();
		}.bind(this, _next));
	}

	// File uploader
	this.UploaderInit = function () {
		this.u = {
				drop_area: this.obj
			};
		this.u.drop_area.ondragenter = function (e) { this.UploaderHandler(e); }.bind(this);
		this.u.drop_area.ondragover = function (e) { this.UploaderHandler(e); }.bind(this);
		this.u.drop_area.ondragleave = function (e) { this.UploaderHandler(e); }.bind(this);
		this.u.drop_area.ondrop = function (e) { this.UploaderHandler(e); }.bind(this);
		
	}
	
	this.UploaderDestroy = function () {
		if (this.obj) {
			this.u.drop_area.ondragenter = null;
			this.u.drop_area.ondragover = null;
			this.u.drop_area.ondragleave = null;
			this.u.drop_area.ondrop = null;
		}
		this.u = null;
	}
	
	this.UploaderHandler = function (e) {
		e.preventDefault();
		e.stopPropagation();
		if (this.eventCallback !== false) {
			this.eventCallback(e.type);
		}
		if (e.type == 'drop') {
			// File droped
			this.log("DEBUG", [ "File droped", e.dataTransfer.files ]);
			if (e.dataTransfer.files[0].type.substring(0,5) == "image") {
				this.log("DEBUG", [ "image format", this.format]);
				var reader = new FileReader();
				reader.onload = function (e) {
					if (this.imageCallback !== false) {
						this.imageCallback(e.target.result, 'img');
					}
					this.log("DEBUG", [ "File done", e.target.result, this.imageCallback !== false ]);
					var that = this;
					var tmp = new Image();
						tmp.onload = function () {
							// check images size
							that.clearPlaceholder();
							that.imageSize = {w: this.naturalWidth, h: this.naturalHeight};
							that.sendMoveEvent = true;
							that.cropUnlock();
							if (that.imageCallback !== false) {
								that.imageCallback('', 'imgdroped');
							}
							that.CroppieInit(this.src);
						};
						tmp.src = e.target.result;
					}.bind(this);
				reader.readAsDataURL(e.dataTransfer.files[0]);
			} else {
				this.log("ERROR", [ "Image", "Wrong image"]);
			}
		}
	}
	
	
	// Croppie 
	
	this.CroppieInit = function (url) {
		if (this.c == null) {
			this.config.croppie = {
				viewport: {
					width: this.config.size.w,
					height: this.config.size.h,
					type: 'square'
				},
				boundary: {
					width: this.config.size.w,
					height: this.config.size.h
				},
				enableExif: true,
				enableOrientation: true,
				showZoomer: false
			};
			this.c = new Croppie(this.obj, this.config.croppie);
			if (this.eventCallback !== false) {
				// process image move event
				this.obj.addEventListener('mousedown',  function () { this.sendMoveEvent = true;  this.mouseButton = true; this.eventCallback('mousedown'); this.updateImagePreview(false); }.bind(this));
				this.obj.addEventListener('mouseup',    function () { this.mouseButton   = false; this.eventCallback('mouseup'); /*this.updateImagePreview(true);*/ }.bind(this));
				this.obj.addEventListener('mouseleave', function () { this.sendMoveEvent = false; this.eventCallback('mouseleave'); }.bind(this));
				this.obj.addEventListener('mouseenter', function () { this.sendMoveEvent = true;  this.eventCallback('mouseleave'); }.bind(this));
				this.obj.addEventListener('mousemove',  function () { 
						if (this.mouseButton && this.sendMoveEvent && !this.cropLocked) {
							this.eventCallback('imagemove');
							this.updateImagePreview(false);
						}
					}.bind(this));
			}
			this._CroppieUpdate = this.CroppieUpdate.bind(this);
			this.obj.addEventListener('update', this._CroppieUpdate);
		}
		if (url) {
			this.imageURL = url;
			this.c.bind(url);
			this.updateZoomLimitEnabled();
		}
	}
	
	this.clearEvents = function () {
		// TODO clearMouseEvents: mousedown, mouseup, mouseleave, mouseenter, mousemove
		this.obj.removeEventListener('update', this._CroppieUpdate);
		this._CroppieUpdate = null;
	}
	
	this.cropUnlock = function () {
		if (this.c !== null) {
			this.cropLocked = this.c.setDisable(false);
		}
	}
	this.cropLock = function () {
		if (this.c !== null) {
			this.cropLocked = this.c.setDisable(true);
		}
	}
	
	this.updateImagePreview = function (flag) {
		if (this.objViewportTimeout) {
			clearTimeout(this.objViewportTimeout);
			this.objViewportTimeout = false;
		}
		if (!this.cropLocked) {
			if (flag) {
				this.objViewportTimeout = setTimeout( function () {
						this._updateImagePreview(true);
					}.bind(this),
					100
				);
			} else {
				this._updateImagePreview(false);
			}
		}
	}
	
	this._updateImagePreview = function (flag, imgCanvas) {
		if (!this.objViewport) {
			this.objViewport = this.obj.getElementsByClassName("cr-viewport")[0];
		}
		if (flag) {
			// update overlay
			if (imgCanvas) {
				this.objViewport.style.backgroundImage = "url(" + imgCanvas.toDataURL() + ")";
				this.objViewportFlag = true;
			} else {
				this.c.result({
					type: 'rawcanvas',
					size: 'original',
					circle: false,
					}).then(function (canvas) {
						var resize_canvas_final = document.createElement("canvas");
						var resize_context_final = resize_canvas_final.getContext("2d");
						resize_canvas_final.width = this.config.size.w;
						resize_canvas_final.height = this.config.size.h;
						this.objViewportPica.resize(canvas,resize_canvas_final).then(function (r) { this._updateImagePreview(true, resize_canvas_final) }.bind(this));
					}.bind(this));
			}
		} else if (this.objViewportFlag == true) {
			// clear overlay
			this.objViewport.style.backgroundImage = "";
			this.objViewportFlag = false;
		}
	}
	
	this.CroppieReinit = function (s) {
		this.config.size.w = parseInt(s.w);
		this.config.size.h = parseInt(s.h);
		if (this.c !== null && s !== false) {
			this.c.destroy();
			this.objViewport = null;
			this.config.croppie = {
				viewport: {
					width: this.config.size.w,
					height: this.config.size.h,
					type: 'square'
				},
				boundary: {
					width: this.config.size.w,
					height: this.config.size.h
				},
				enableExif: true,
				enableOrientation: true,
				showZoomer: false
			};
			this.c = new Croppie(this.obj, this.config.croppie);
			if (this.imageURL) {
				this.c.bind(this.imageURL);
				this.updateZoomLimitEnabled();
				this.cropLocked = false;
			}
		}
	}
	
	this.CroppieUpdate = function (e) {
		var updatePoints = false;
		if (!this.cropLocked) {
			if (this.zoomLimitEnabled && e.detail.zoom > 1) {
				// prevent zoom more than 100%
				this.c.setZoom(1);
			}
			if (this.imageCallback !== false) {
				this.c.result({
						type: 'base64',
						format: "png",
						circle: false,
					}).then(function (b64) {
						this.imageCallback(b64, 'img');
					}.bind(this));
			}
			this.updateImagePreview(false);
			this.updateImagePreview(true);
		}
	}
	
	this.updateZoomLimitEnabled = function () {
		if (this.config.size.w > this.imageSize.w || this.config.size.h > this.imageSize.h) {
			this.zoomLimitEnabled = false;
		} else {
			this.zoomLimitEnabled = true;
		}
	}


	// Crop
	
	this.crop = function (_callback) {
		this.callback = _callback;
		if (this.c !== null) {
			this.c.result({
				type: 'rawcanvas',
				size: 'original',
				circle: false,
				}).then(function (canvas) {
					var resize_canvas_final = document.createElement("canvas");
					var resize_context_final = resize_canvas_final.getContext("2d");
					resize_canvas_final.width = this.config.size.w;
					resize_canvas_final.height = this.config.size.h;
					var pica = Pica();
					pica.resize(canvas,resize_canvas_final).then(function (r) { this.callback(resize_canvas_final) }.bind(this));
				}.bind(this));
		} else {
			this.callback(false);
			this.log("ERROR", [ "Submit", "Croppie was not initialized" ]);
		}
	}

	// LOG handler
	
	this.log = function (s,m) {
		// TODO log levels by `s`
		console.log ("BoboCrop:", s, m);
	}
	
	
	// init
	this.init(obj);
}

export default Bobocrop;
