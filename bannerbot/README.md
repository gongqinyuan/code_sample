Bannerbot Core v2
=================

## Laravel backend

## Vue.js frontend
 - `croppie` - crop tool
 - `html2canvas` locked to 1.0.0-alpha.12 version (upgrade only after success tests with all vendors banners)
 - `pica` - image manipulation (resize) library

## Install
 - clone bannerbot core repository
 - `composer install`
 - `npm install`
 - setup .env
 - Deploy vendor repository
 - `npm run development` (for dev environment) or `npm run production` (for production)
 - `composer dump-autoload -o && php artisan optimize` - Laravel optimization
 - `php artisan migrate` - create DB structure
 - `php artisan db:seed` - add user groups into DB (ones)
 - `php artisan initvendor:db` - init vendor configuration and create tables for that (ones)
 - setup Google credentials

### Import/Export
 - `php artisan import:google` - import data from Google Docs (from `GOOGLE_SPREADSHEET_ID_IMPORT`), `active` banner will be published, banners with `ID=0` will be duplicated on every request!!!. Should be used ones to import data from bannerbot v1 version
 - `php artisan sync` - export data to Google Docs (to `GOOGLE_SPREADSHEET_ID_EXPORT`)
 

### Cron task
Cron job will run every minute to stay sync with Google Docs (see `php artisan sync`)

`* * * * * cd /path/to/your/project && php artisan schedule:run > /dev/null 2>&1`

To prevent task overlap, the Laravel `withoutOverlapping` method using with 5 minutes timeout. After every deploy it is normal if synchronization task (DB -> GoogleDocs) not run up to 5 minutes.


### Setup Google credentials and export data
 - Copy credentials.json into root of the project
 - `php artisan import:google` and follow instructions. This will setup Google token and import data from previews version.

### User registration
User can be registered via `/register` route. Registration only available for IP addresses in white list defined in `.env` in `REGISTRATION_WHITELIST_IP`

### Add user
Use `php artisan tinker`:
```
$user = new App\User();
$user->password = Hash::make('admin');
$user->email = 'admin@bannerbot.co.nz';
$user->name = 'Admin';
$user->save();
```

### Add user to group
Available group names (by default) are `Admin`, `Client` or `Media`.
```
$group = App\group::where("name", "GROUP NAME")->first();
$ug = new App\users_group();
$ug->user_id = ID OF CREATED USER
$ug->group_id = $group->id;
$ug->save();
```

**REMOVE HISTORY FILE** after that: `~/.config/psysh/psysh_history`


## Deploy vendor repository
Vendor files and settings can be deployed as `npm run vendor repository branch`, e.g. `npm run vendor 'git@bitbucket.org:rapptribalnz/bannerbot-vendor.git' 'develop-vendor'`

Vendor repository should be available from server via SSH Keys without password prompt.


## Environment

| Variable                     | Description                                                                             |
|------------------------------|-----------------------------------------------------------------------------------------|
| GOOGLE_CREDENTIALS           | Path to `credentials.json` file                                                         |
| GOOGLE_ACCESS_TOKEN_PATH     | Path to token file, e.g. `tokens/google.token`, folder should be `rwx` for project user |
| GOOGLE_SPREADSHEET_ID_EXPORT | ID of the Google Spreadsheet to export (should be used by AR Studio), not equal to `GOOGLE_SPREADSHEET_ID_IMPORT` |
| GOOGLE_SPREADSHEET_ID_IMPORT | ID of the Google Spreadsheet for data import from v1 bannerbot                          |
| TINIFY_API_KEY               | API KEY for tinepng/tinyjpg                                                             |
| AWS_ACCESS_KEY_ID            | AWS key                                                                                 |
| AWS_SECRET_ACCESS_KEY        | AWS secret                                                                              |
| AWS_DEFAULT_REGION           | AWS region, e.g. `ap-southeast-2`                                                       |
| AWS_BUCKET                   | AWS S3 folder, e.g. `bbanners/`                                                         |
| DB_HOST                      | DB host                                                                                 |
| DB_USERNAME                  | DB username                                                                             |
| DB_PASSWORD                  | DB password                                                                             |
| DB_DATABASE                  | DB name                                                                                 |
| CACHE_ID                     | Avoid assets caching                                                                    |
| MAIL_DRIVER                  | should be set to "sendgrid"                                                             |
| SENDGRID_API_KEY             | SendGrid API key                                                                        |
| MAIL_FROM_ADDRESS            | email address sender                                                                    |
| MAIL_FROM_NAME               | sender name, e.g. "BannerBot"                                                           |
| REGISTRATION_WHITELIST_IP    | IP where Registration form available, separated by comma, e.g. "192.168.1.1,127.0.0.1"  |

## Errors

 - `Data source service unavailable` - wrong credentials or unavailable Google API
 - `Data source authorization required` - in terminal execute PHP script `gd_init.php` in `src/_useful/` and follow instructions


## Access
Table `groups` and `groups_access` restrict access to API methods, some UI and routes.

| users     | users_groups | groups    | groups_access     |
|-----------|--------------|-----------|-------------------|
| is_active |              | is_active |                   |
| `id`      | `user_id`    |           |                   |
|           | `group_id`   | `id`      | `group_id`        |
|           |              |           | access            |
|           |              |           | r - GET requests  |
|           |              |           | w - POST requests |
|           |              |           | x - special       |

### Init route
`groups`.`init` - set path to use as default for users of group.

### Groups access

| API/Access/Route   | Read (r) | Write (w) | Special (x) | Notes                                                                          |
|--------------------|:--------:|:---------:|:-----------:|--------------------------------------------------------------------------------|
| `/admin/items/`    |          |           |     `+`     | enable/disable route to banners                                                |
| `/admin/overview/` |          |           |     `+`     | enable/disable route to overview                                               |
| `_overviewbanner`  |          |           |     `+`     | show/hide banner create button in overview                                     |
| `overview`         |    `+`   |    `+`    |     `+`     | `banners` read required for preview                                            |
| `cats`             |    `+`   |           |             | Cars list                                                                      |
| `bannersizes`      |    `+`   |           |             | Sizes of banners for selected car                                              |
| `banners`          |    `+`   |    `+`    |     `+`     |                                                                                |
| `deleteBanners`    |          |    `+`    |             |                                                                                |
| `uploadImage`      |          |    `+`    |             | Should be enabled with `banners` write                                         |


# Settings configuration
BannerBot configurations is very flexible.

Settings can be configured in DB:

 * Table "settings" -> bannersizes (json)
 * Table "settings" -> bannersSettings (json)
 * Table "settings" -> exportSettings (json)
 * Table "settings" -> overviewSettings (json)
 * Table "settings" -> lastSync (json)

## exportSettings structure

 * `items` array:
    * Banner sizes array: `300x250`, `300x600` and `970x250` (should be the same as for `bannersizes` config in DB)
       * `field` - field name related to data storage, e.g. GoogleDrive

## overviewSettings structure

 * `items` array:
    * `field` - field name related to data storage, e.g. GoogleDrive
    * `title` - title of the field in Overview section of BannerBot
    * `type` - type of the field:
       * `category`
       * `bannername`
       * `ad_id`
       * `active`
       * `clickthrough_url`


## bannersSettings structure

 * `items` array:
    * Banner sizes array: `300x250`, `300x600` and `970x250` (should be the same as for `bannersizes` config in DB)
       * `field` - field name related to data storage, e.g. GoogleDrive
       * `type` - type of the field: 
          * `id`, 
          * `boolean`, 
          * `active`, 
          * `string`,
          * `integer`, 
          * `bannername`, 
          * `category` (e.g. Car Model), 
          * `ad_id`,
          * `clickthrough_url`,
          * `array` - array in the code, for data storage it usually string with YYNY, where Y = true and N = false
          * `color_main` - main color for some vendor specific settings
          * `frames`
       * `fields` (for `frames`): 
          * `field`
          * `type` - equal to parent `type` field
          * `check` - (bool) check if field has been update and should be save
          * `inchainDisabled` - (bool) if all fields in chain to check if banner disabled
       * `settings` (for `frames`):
          * `field` - all settinfs variables should start with `_` and can be used in code
             * `_enabled` - banner enabled/disabled
             * `_type` - type of the banner
             * `_wedge` (for some vendors)
          * `type` - type of the field:
             * `YN` - "(Y)es" or "(N)o", almost as boolean for banner where state as string like "YYYNNY" and with type `array`
             * `type` - type of the banner, the `value` should be:
                * `imgtxt` - for banner with copy and croping tool (`300x600` and `970x250`)
                * `imageortext` - for banners that display as set of frames, one copy-only, next image only (`300x250`)
          * `value` - default value, e.g. "true" ("true" as string, not a boolean)
          * `parent` - parent `field`, e.g. "Show_Frame"
          * `parent_value` - parent default value, e.g. "Y"
          * `check` - (bool) check if field has been update and should be save


# Vendor specific features

```
+---------------------------------------------------------------------+
|                 Menu                                                |
+---------------------------------------------------------------------+
|                                                                     |
|   [banner selector]                    [BannersVendor]  [buttons]   |
|                                                                     |
|   +----------------------------+   +----------------------------+   |
|   |      [BannerItemVendorTop] |   |      [BannerItemVendorTop] |   |
|   +----------------------------+   +----------------------------+   |
|   |                            |   |                            |   |
|   |  [BannerItemVendorBanner]  |   |  [BannerItemVendorBanner]  |   |
|   |                            |   |                            |   |
|   +----------------------------+   +----------------------------+   |
|                                                                     |
+---------------------------------------------------------------------+
```

## Folder structure
```
 [root]
   |
   +-- [resources/vendor/] - vendor source code
   |     |
   |     +-- [js/] - vendor JavaScript files and VUE components
   |     |     |
   |     |     +--[js/shared.js] - shared functions and data
   |     |
         +-- [settings/] - set of JSON files with vendor settings
   |     |
   |     +-- [css/] - vendor styles
   |
   +-- [public/src/] - vendor public components
         |
         +-- [img/] - images, icons, sprites, etc
         |
         +-- [preview/] - banner previews
```

## Events
### Banners plugins
EventBus using for communication between plugins with channel `updateBannersVendorValue`.

Payload is the JSON object with the following fields:

 * `frame` - `false` for global values and ID of the Frame for frame values
 * `values` - values to be set/updated: 
     * `type` - type of the values set (could be empty):
         * `framecontentupdate` - content of the frame have been updated (e.g. user input, frame(non-global) color changes)
         * `vendorcolor` - global vendor color update
     * `values` - array of the values to be updated:
         * `key` - key of the banner parameter, for some value special events will be apply (see below `cta`)
         * `value` - value of parameter
 * `actions` (for frames):
     * `action` - action name:
        * `addClass` - add class to banner frame
        * `removeClass` - remove class from banner frame
        * `reInitCrop` - reinit cropping tool if size of the image area was changed (do this AFTER all frame changes), pass size object with width and height of crop area {w: xxx, h: yyy}
        * `initFrame` - fully init frame with crop, screenshot and upload functions (depends of frame), pass size object with width and height of crop area {w: xxx, h: yyy}
        * `checkPreState` - validate changes state (compare to init state), this should be done to update "save" state
     * `values` - values array that should be pass to frame
        * `key` - empty for `addClass` and `removeClass`
        * `value`

### Banner frame fields
For sending data from vendor banner frame fields to banner data object fields use the same `updateBannersVendorValue` event.
Payload is the JSON object with the following fields:

 * `frame` - ID of the Frame for frame data values
 * `values`
    * `type`
    * `values` - array of the values to be updated:
       * `key` - key of the banner parameter
          * `cta` key used to update global `CTA_Text` value (and that is only used for it at that moment)
       * `value` - value of banner data field

## Shared methods
`shared` from 'vendor/shared'

Set of the shared methods used across other vendor modules and main banneritem:

 - `cropSize` - size of the crop tool depends on the banner settings
 - `openUserGuide` - open user guide/manual, e.g. use `windows.open(...)` to open some useful URL
 - `screenshotPreAction` - prepare item for screenshot (e.g. add extra class)
 - `screenshotPostAction` - actions after screenshot have been done (e.g. remove extra class)
 - `bannersNumber` - return number of banner frames
 - `prepareData` - prepare banner frames data for preview, e.g. replace newline to <br> tag


## Banners
`BannersVendor` from 'vendor/js/banners'

Component to update global banner settings. Sit on the top of the Banner page.


## Banner items
`BannerItemVendorTop` from 'vendor/js/banneritem.top'

Component to update frame settings (except enable/disable as it is global). Sit on the top of banner frame.


## Banner frame
`BannerItemVendorBanner` from 'vendor/js/banneritem.banner'

Banner frame and all it's logic.

## Envoyer hooks

### 1. Install NPM Modules
```
cd {{release}}

npm install
```

### 2. Run Laravel optimization
```
cd {{release}}

composer dump-autoload -o && php artisan optimize
```

### 3. Get vendor repo
```
cd {{release}}

npm run vendor 'git@bitbucket.org:rapptribalnz/bob-banners.git' 'BRANCH'
```

### 4. Run NPM build
```
cd {{release}}

npm run production
```

### 5. Remove NPM Modules
```
cd {{release}}

rm -R ./node_modules/
```

### 6. Google Docs API
Expected that previews credentials exists
```
cp {{project}}/storage/vault/credentials.json {{release}}/
cp {{project}}/storage/vault/google.token {{release}}/tokens/
```

### 7. Migrate DB
This can be skipped for production deploy and needs to be run manually
```
cd {{release}}

php artisan migrate
```

# Migration from v1
 - DB user should has REFERENCES permission (Sitehost)
 - (should be already done in `bob-banners` repository)copy DB settings into JSON files named as `key_from_db_settings_table.json` and put it into vendor/settings folder of vendor package/repository: bannersizes.json, bannersSettings.json, cats.json, exportSettings.json, overviewSettings.json, vendorSettings.json
 - duplicate GoogleDocs table and use it as `GOOGLE_SPREADSHEET_ID_IMPORT`
 - prepare deployment tool (envoyer hooks), including correct deploy.js parameters and `.env`
 - BannerBot v1 GoogleDocs credentials and token files can be used
 - clear DB (remove all tables before `php artisan migrate` and `php artisan initvendor:db`)
 - deploy
 - after successful deploy, run `php artisan migrate` (if it has not be executed on deployment), than run `php artisan db:seed` to add all user groups and access rights
 - run `php artisan initvendor:db` - to init DB structure (should be run only ones manually or after DB structure update)
 - DON'T need to to this if both Google Docs equal and all banners published. Clear original GoogleDoc `GOOGLE_SPREADSHEET_ID_EXPORT` (check DoubleClick/Google Studio schedule before to avoid Studio fetching empty Google Doc until cron task)
 - run `php artisan import:google` - to import v1 Bannerbot GoogleDoc into v2 DB (check and update if any records have zero ID, run it only ones), any `active` banners will be pushed to `GOOGLE_SPREADSHEET_ID_EXPORT` table
 - add Admin user (and other users that required)
 - update crontab file (add cron task), note that cron task can freeze for up to 5 minutes after every deploy: `* * * * * cd /container/application/current/ && php artisan schedule:run > /dev/null 2>&1`
 - update Apache2 config (`public` instead of `public_html`) and restart container
