const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig(webpack => {
	return {
		module: {
			defaultRules: [
				{
					type: 'javascript/auto',
					resolve: {}
				},
				{
					test: /\.json$/i,
					type: 'json'
				},
				{
					test: /\.wasm$/,
					loaders: ['wasm-loader']
				}
			]  
		},
		optimization: {
			occurrenceOrder: true // To keep filename consistent
		},
		plugins: [
			new webpack.DefinePlugin({
				BANNERBOT_VERSION: JSON.stringify(require("./package.json").version)
			}),
		],
		output: {
			chunkFilename: 'js/chunks/[name].[chunkhash].js',
		}
		};
	})
	.js('resources/js/admin.js', 'public/js')
	.sass('resources/sass/admin.scss', 'public/css');
