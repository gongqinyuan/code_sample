# Bannerbot core API

## Overall
All correct API responses is JSON objects with strict structure:

 - `success` (boolean) - result status, can be `true` or `false`
 - `message` (string|array|object) - result payload

```javascript
{
   "success":true,
   "message":{
   ...
   }
}
```

## ACL (Access Control List)
**GET** `/api/acl`

Return access control list with (r)ead, (w)rite and e(x)ecute properties to build routes available for current user and special `_init_` value indicate default route (see "Groups access").

Example:

```javascript
{
  "success": true,
  "message": {
    "_init_": "/admin/overview\",
    "_overviewbanner": {"r":false,"w":false,"x":true},
    "/admin/items/": {"r":false,"w":false,"x":true},
    ...
    "uploadImage":{"r":true,"w":true,"x":false}
  }
}
```

## Category
### Get categories list
**GET** `/api/category`

Return `items` array with category items:

 - `ID` - internal item ID
 - `img` - image URL
 - `title` - item title

Example:

```javascript
{
  "success": true,
  "message": {
    "items": [
      {"title":"Model1","ID":"model1","img":"/src/img/models/model1.png"},
      {"title":"Model2","ID":"model2","img":"/src/img/models/model2.png"},
      ...
      {"title":"General","ID":"general","img":"/src/img/icons/generic.png"}
    ]
  }
}
```

### Get category items
**GET** `/api/category/{category_id}/{banner_size}`

Example:

```javascript
{
  "success": true,
  "message": {
    "1":{"ID":1,"title":"Model1_A"},
    ...
    "5":{"ID":5,"title":"Model1_B"}
  }
}
```

## Overview
### Get overview settings
**GET** `/api/overviewSettings`

Return `items` array with overview items fields (see "overviewSettings structure"):

 - `field` - field name in database
 - `title` - field title
 - `type` - field type

Example:

```javascript
{
  "success": true,
  "message": {
    "items": [
      {"field":"Field_Model","title":"Model","type":"category"},
      ...
      {"field":"Banner_Name","title":"Banner name","type":"bannername"}
    ]
  }
}
```

### Get overview items
**GET** `/api/overview/{banner_size}`

Example:

```javascript
{
  "success": true,
  "message": [
    {"ID":1,"Banner_Name":"Banner for Model1","Car_Model":"Model1","CM_Ad_ID":2,"Backup_Image_URL":"https://domain/image1.png","ClickThrough_URL":"https://bannerbot.co.nz/","_published":false},
    ...
    {"ID":99,"Banner_Name":"Model2","Car_Model":"Model2","CM_Ad_ID":32345,"Backup_Image_URL":"https://domain/imageB.png","ClickThrough_URL":"https://domain/","_published":true}
  ]
}
```

## Update overview item(s)
**PUT** `/api/overview/{banner_size}`

Parameter `data` as two dimensional array where first index is overview item ID and the second is field name, e.g. `data[2][ClickThrough_URL] = "http://example.co.nz/?foo=bar"`.

Example:

```javascript
{
  "success":true,
  "message":true
}
```

## Banner
### Banner sizes
**GET** `/api/bannersizes`

Return `items` array banners sizes items:

 - `title` - banner size title, e.g. "300x250"
 - `ID` - banner size ID (`banner_size`), usually the same as `title`
 - `w` - banner width
 - `h` - banner height
 - `frames` - number of frames for that banner size

Example:

```javascript
{
  "success": true,
  "message": {
    "items": [
      {"title":"300x250","ID":"300x250","w":"300","h":"250","frames":"7"},
      {"title":"300x600","ID":"300x600","w":"300","h":"600","frames":"4"},
      {"title":"970x250","ID":"970x250","w":"970","h":"250","frames":"4"}
    ]
  }
}
```

### Banner settings
**GET** `/api/bannersSettings`

Return `items` object with banner settings, where key of items is `banner_size`. Banner settings is an array of object with following fields:

 - `field` - field name in database
 - `type` - type of the field

Example:

```javascript
{
  "success": true,
  "message": {
    "items": {
      "300x250":[
        {"field":"ID","type":"id"},
        ...
        {"field":"Frames","type":"frames",
          "fields":[
          {"field":"Heading","type":"string","check":true,"inchainDisabled":true},
          ...
          {"field":"Image","type":"string"}
          ],
          "settings":[
           {"field":"_enabled","type":"YN","value":"true","parent":"Show_Frame","parent_value":"Y","check":true},
           {"field":"_type","type":"type","value":"imageortext"}
          ]
        }
      ],
      "300x600":[
      ...
      ]
    }
  }
}
```

### Banner versions
**GET** `/api/banner/{banner_size}/{ID}`

Example:

```javascript
{
  "success": true,
  "message": {
    "1":{"ID":1,"title":"Version: Draft"},
    "2":{"ID":2,"title":"Version: Published"}
  }
}
```

### Create new banner
**POST** `/api/banner/{banner_size}`

Fields:

 - `bannerName` - title of the banner
 - `adID` - ID of the banner for Studio
 - `category` - `category_id` of the banner

### Get banner
**GET** `/api/banner/{banner_size}/{ID}/{version}`

Example:
```javascript
{
  "success": true,
  "message": {
    "ID":1,
    "Banner_Name":"Banner for Model1",
    "Car_Model":"Model1",
    "Show_Frame":["Y","Y","Y","Y","Y","Y","Y"],
    "Frames":[
      {"_ID":1,"Heading":"Header","Sub":"sub header","Terms":""},
      {"_ID":2,"Image":"https://domain/path/name.jpg"},
      ...
      {"_ID":7,"Heading":"Header","Sub":"Last frame","Terms":"Term and"}
    ]
  }
}
```

### Update banner
**PUT** `/api/banner/{banner_size}/{ID}`

Update current banner (draft) with edited data.

### Delete banner
**DELETE** `/api/banner/{banner_size}/{ID}`

Remove banner

### Publish banner
**POST** `/api/publish/{banner_size}/{ID}`

Put banner's current (draft) state into the published state (should be called after "Update banner"). Banner will be published with cron task.

### Unpublish banner
**DELETE** `/api/publish/{banner_size}/{ID}`

Remove banner from published state. Banner will be unpublished with cron task.


## Other methods
### Download banner backup image
**GET** `/api/download`

Parameters:

 - `url` - URL of the image in S3,
 - `filename` - customized filename for file

Return file with headers to correctly download file:

```
Content-Description: File Transfer
Content-Type: application/octet-stream
Content-Disposition: attachment; filename="filename"
Expires: 0
Cache-Control: must-revalidate
Pragma: public
Content-Length: 23459
```

### Upload banner images
**POST** `/api/upload`

Upload banner image encoded into Tinify to optimize image and reduce size and store it in AWS S3 Bucket.

Parameters:

 - `img` - binary file data send as form

Example:

```javascript
{
  "success": true,
  "message": "https://domain/path/name.jpg";
}
```

```javascript
{
  "success": false,
  "message": {
    "error": "Error message"
  }
}
```