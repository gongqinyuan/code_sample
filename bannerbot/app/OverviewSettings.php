<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Settings;

class OverviewSettings extends Model
{
	static function get() {
		return Settings::get('overviewSettings');
	}
	
	static function getFields() {
		$settings = self::get();
		$fields = [];
		foreach ($settings['items'] as $item) {
			$fields[] = $item['field'];
		}
		return $fields;
	}
	static function getFieldsDB($prefix) {
		$settings = self::get();
		$fields = [];
		foreach ($settings['items'] as $item) {
			$fields[] = $prefix.$item['field']." AS ".$item['field'];
		}
		return $fields;
	}
}
