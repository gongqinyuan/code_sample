<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Settings;

class BannersSettings extends Model
{
	static function get() {
		return Settings::get('bannersSettings');
	}
	
	static function getFieldByType($bannerSize, $type) {
		$fields = self::get();
		if (isset($fields['items']) && isset($fields['items'][$bannerSize])) {
			foreach ($fields['items'][$bannerSize] as $field_name => $item) {
				if ($item['type'] == $type) {
					return $item['field'];
				}
			}
		}
		return false;

	}
	
	static function getIdField($bannerSize) {
		return self::getFieldByType($bannerSize, "category");
	}

	static function getAdIdField($bannerSize) {
		return self::getFieldByType($bannerSize, "ad_id");
	}
	
	static function getActiveField($bannerSize) {
		return self::getFieldByType($bannerSize, "active");
	}
	
	static function getBannerNameField($bannerSize) {
		return self::getFieldByType($bannerSize, "bannername");
	}
	
	static function getClickthroughUrlField($bannerSize) {
		return self::getFieldByType($bannerSize, "clickthrough_url");
	}
}
