<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
	static private function dt() {
		return date("Y-m-d H:i:s");
	}

	static function get($key) {
		$item = \DB::table("settings")->select("v", "vtype")->where("k", $key)->first();
		if ($item) {
			return $item->vtype == 'json'?json_decode($item->v,true):$item->v;
		}
		return false;
	}
	static function set($key, $value) {
		// TODO validate data type
		try {
			\DB::table("settings")->where("k", $key)->update(["updated_at" => self::dt(), "v" => is_array($value)?json_encode($value):$value]);
			return true;
		} catch (\Exception $e) {
			
		}
		return false;
	}
}
