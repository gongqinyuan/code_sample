<?php

namespace App;

class GoogleDocs
{
	private $client = false;
	private $service = false;
	private $source_id = false;
	private $last_error = false;
	private $meta = [];

	function __construct($source_id) {
		try {
			$this->_initClient();
			$this->_initService();
			$this->source_id = $source_id;
		} catch (Exception $e) {
			return false;
		}
	}

	private function _initService() {
		$this->service = new \Google_Service_Sheets($this->client);
	}

	private function setError($e) {
		$this->last_error = $e;
	}
	function getError () {
		if ($this->last_error !== false) {
			return $this->last_error;
		}
		return false;
	}

	private function _initClient() {
		// https://developers.google.com/sheets/api/quickstart/php
		$this->client = new \Google_Client();
		$this->client->setApplicationName('Bannerbot');
		$this->client->setScopes(\Google_Service_Sheets::SPREADSHEETS);
		$this->client->setAuthConfig(__DIR__ . '/../'.config('googledocs.CREDENTIALS'));
		$this->client->setAccessType('offline');
		$this->client->setPrompt('select_account consent');

		// Load previously authorized token from a file, if it exists.
		$tokenPath = __DIR__.'/../'.config('googledocs.ACCESS_TOKEN_PATH');
		if (file_exists($tokenPath)) {
			$accessToken = json_decode(file_get_contents($tokenPath), true);
			$this->client->setAccessToken($accessToken);
		}

		// If there is no previous token or it's expired.
		if ($this->client->isAccessTokenExpired()) {
			// Refresh the token if possible, else fetch a new one.
			if ($this->client->getRefreshToken()) {
				$this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
			} else {
				try {
					if (php_sapi_name() == "cli") {
						// Request authorization from the user.
						// TODO, what about client?
						$authUrl = $this->client->createAuthUrl();
						printf("Open the following link in your browser:\n%s\n", $authUrl);
						print 'Enter verification code: ';
						$authCode = trim(fgets(STDIN));

						// Exchange authorization code for an access token.
						$accessToken = $this->client->fetchAccessTokenWithAuthCode($authCode);
						$this->client->setAccessToken($accessToken);

						// Check to see if there was an error.
						if (array_key_exists('error', $accessToken)) {
							throw new Exception(join(', ', $accessToken));
						}
					} else {
						throw new Exception("no CLI");
					}
				} catch (Exception $e) {
					$this->setError(["code"=>500, "message"=>"Data source authorization required"]);
					return false;
				}
			}
			// Save the token to a file.
			if (!file_exists(dirname($tokenPath))) {
				mkdir(dirname($tokenPath), 0700, true);
			}
			file_put_contents($tokenPath, json_encode($this->client->getAccessToken()));
		}
	}

	private function _getCols ($table) {
		$response = $this->service->spreadsheets_values->get($this->source_id, $table."!A1:ZZZ1");
		$values = $response->getValues();

		$cols = [];
		for ($i = 0; $i < count($values); $i++) {
			if ($i == 0) {
				$cols = $values[$i];
			}
		}
		return $cols;
	}

	private function _sheetId($table) {
		try {
			$sheetId = false;
			$sheets=$this->service->spreadsheets->get($this->source_id)->getSheets();
			foreach($sheets as $sheet) {
				if($sheet->properties->title == $table) {
					$sheetId = $sheet->properties->sheetId;
					break;
				}
			}
			return $sheetId;
		} catch (Exception $e) {
			return false;
		}
	}

	function getMeta() {
		return $this->meta;
	}


	function get ($table) {
		if (!$this->service || !$this->service->spreadsheets_values) {
			$this->setError(["code"=>500, "message"=>"Data source service unavailable"]);
			return false;
		}
		$sheetId = $this->_sheetId($table);
		$response = $this->service->spreadsheets_values->get($this->source_id, $table);
		$values = $response->getValues();

		// Process data
		$data = [];
		$cols = [];
		for ($i = 0; $i < count($values); $i++) {
			if ($i == 0) {
				$cols = $values[$i];
			} else {
				$data[$i-1]["_URL"] = [
						"id"  => $this->source_id,
						"gid" => $sheetId,
						"range" => "A".($i+1).":G".($i+1)
					];
				for ($j = 0; $j < count($cols); $j++) {
					if (!isset($data[$i-1])) $data[$i-1] = [];
					$data[$i-1][$cols[$j]] = isset($values[$i][$j])?$values[$i][$j]:NULL;
				}
			}
		}
		$this->meta = [
			"table" => $table,
			"items_in_table" => count($data)
		];
		unset($cols);
		return $data;
	}

	function set($table, $data, $retry = 3) {
		// this method will replace ALL data in table
		// get current table state
		$current_data = $this->get($table);
		$new_data = [];
 		$field_num = 1;	// replace header (if something goes wrong, the wrong header probably stop it to push into Studio), 1 is the first row
		foreach ($data as $item) {
			$new_data[] = new \Google_Service_Sheets_ValueRange(
				array(
					'range' => $table."!A".$field_num,
					'values' => [ array_values($item) ]
				)
			);
			$field_num++;
		}

		// fill other fields with empty data
		$dummy_from = $field_num;
		for ($dummy_item = $dummy_from; $dummy_item <= count($current_data)+1; $dummy_item++) {
			$new_data[] = new \Google_Service_Sheets_ValueRange(
				array(
					'range' => $table."!A".$field_num,
					'values' => [array_fill(0, count($data[0]), "")]	// fill all other rows with empty string data
				)
			);
			$field_num++;
		}

		// Update Google Docs
		$result = $this->service->spreadsheets_values->batchUpdate(
			$this->source_id,
			new \Google_Service_Sheets_BatchUpdateValuesRequest([
				'valueInputOption' => "USER_ENTERED",
				'data' => $new_data
			])
		);

		// Validate number of rows
		// $data include header, that is why count-1
		$current_data = $this->get($table);
		if (count($current_data) != count($data)-1) {
			if ($retry > 0) {
				$retry--;
				$this->set($table, $data, $retry);
			}
		} else {
			return true;
		}
		return false;
	}
}
