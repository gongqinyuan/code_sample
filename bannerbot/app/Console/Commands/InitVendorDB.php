<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BannerSizes;
use App\BannersSettings;
use App\ExportSettings;
use App\Settings;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Log;


class InitVendorDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initvendor:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init vendor DB structure based on settings';

	protected $prefix;
	protected $postfix_history;
	protected $postfix_published;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
		$this->prefix = config("storage.prefix");
		$this->postfix_history = config("storage.postfix_history");
		$this->postfix_published = config("storage.postfix_published");

        parent::__construct();
    }

    /**
     * Update structure of the table
     *
     * @return boolean
     */
     
    private function initVendorSettings() {
		$settings = ['bannersizes', 'bannersSettings', 'cats', 'exportSettings', 'overviewSettings', 'vendorSettings'];
		$path = base_path("resources/vendor/settings");
		foreach ($settings as $item) {
			$item_path = $path."/".$item.".json";
			if (file_exists($item_path)) {
				try {
					$json = json_decode(file_get_contents($item_path));
					if ($json) {
						if (!Settings::set($item, json_encode($json))) {
							Log::error("Unable to set settings", ['item' => $item]);
							return false;
						}
					} else {
						Log::error("Settings file is not JSON", ['file' => $item_path]);
						return false;
					}
				} catch (\Exception $e) {
					Log::error("Settings initialization, unexpected issue", ['error' => $e->getMessage()]);
					return false;
				}
			} else {
				Log::error("Settings file is not exists", ['file' => $item_path]);
				return false;
			}
		}
		return true;
	}
    
    /**
     * Update structure of the table
     *
     * @return boolean
     */
    
    private function updateTableStructure($table_name, $size, $structure, $exportFields) {
		return Schema::table($table_name, function (Blueprint $table) use (&$table_name, &$size, &$structure, &$exportFields) {
			foreach ($structure[$size['ID']] as $field) {
				$export_field_name = $field['field'];
				$field_name = $this->prefix.$export_field_name;
				if ($export_field_name != "Frames" && !in_array($export_field_name, $exportFields[$size['ID']])) {
					// field is not in export list, no need to add it
					// TODO check and remove? "->change()"
				} else if (Schema::hasColumn($table_name, $field_name)) {
					// column exists
					// TODO check and update column type?
				} else {
					switch ($field['type']) {
						case 'id':
							// skip, as DATA_ID should be already created
							break;
						case 'boolean':
							// boolean value
							$table->boolean($field_name)->default(false);
							break;
						case 'active':
							// boolean value
							$table->boolean($field_name)->default(false);
							break;
						case 'ad_id':
							$table->unsignedBigInteger($field_name)->default(0);
							break;
						case 'frames':
							// handle frames
							for ($frame = 1; $frame <= $size['frames']; $frame++) {
								// and frame fields
								foreach ($field['fields'] as $frame_field) {
									$frame_export_field_name = "Frame".$frame."_".$frame_field['field']; 
									$frame_field_name = $this->prefix.$frame_export_field_name;
									if (!in_array($frame_export_field_name, $exportFields[$size['ID']])) {
										// field is not in export list, no need to add it
										// TODO check and remove?
									} else if (Schema::hasColumn($table_name, $frame_field_name)) {
										// column exists
										// TODO check and update column type?
									} else {
										// create fields by type
										switch ($frame_field['type']) {
											case 'boolean':
												$table->boolean($frame_field_name)->default(false);
												break;
											default:
												$table->string($frame_field_name, 255)->nullable();
										}
									}
								}
							}
							break;
						default:
							$table->string($field_name, 255)->nullable();
					}
				}
			}
		});
	}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		// Init vendor settings
		if (!$this->initVendorSettings()) {
			return false;
		}
		
		$sizes = BannerSizes::get()['items'];
		$structure = BannersSettings::get()['items'];
		$exportFields = ExportSettings::getFields();
		DB::transaction(function () use (&$sizes, &$structure, &$exportFields) {
			foreach ($sizes as $size) {
				$table_name = $this->prefix.$size['ID'];
				$table_name_history = $this->prefix.$size['ID'].$this->postfix_history;
				$table_name_published = $this->prefix.$size['ID'].$this->postfix_published;
				
				if (!Schema::hasTable($table_name)) {
					// Create table for current (not published and not history, most time draft or equal to published)
					Schema::create($table_name, function (Blueprint $table) {
						$table->bigIncrements('DATA_ID');	// ID is mandatory field
						$table->timestamps();
						$table->softDeletes();
						$table->unsignedBigInteger('user_id')->default(0);
						$table->boolean("published")->default(false)->index('published');
					});
				}
				if (!$this->updateTableStructure($table_name, $size, $structure, $exportFields)) {
					// TODO cancel transaction
				}
				
				if (!Schema::hasTable($table_name_history)) {
					// History data
					Schema::create($table_name_history, function (Blueprint $table) {
						$table->bigIncrements('id');	// ID is mandatory field
						$table->timestamps();
						$table->softDeletes();
						$table->unsignedBigInteger('user_id')->default(0);
						$table->unsignedBigInteger('history_user_id')->default(0);
						$table->dateTime('history_at')->nullable();
						$table->boolean("published")->default(false);
						$table->unsignedBigInteger("DATA_ID")->default(0)->index("data_id");
					});
				}
				if (!$this->updateTableStructure($table_name_history, $size, $structure, $exportFields)) {
					// TODO cancel transaction
				}
				
				if (!Schema::hasTable($table_name_published)) {
					// Live banners data (published), moslty the same as draft table
					Schema::create($table_name_published, function (Blueprint $table) {
						$table->unsignedBigInteger('DATA_ID')->primary();
						$table->timestamps();
						$table->unsignedBigInteger('user_id')->default(0);
						$table->unsignedBigInteger('publish_user_id')->default(0);
						$table->dateTime('published_at')->nullable();
					});
				}
				if (!$this->updateTableStructure($table_name_published, $size, $structure, $exportFields)) {
					// TODO cancel transaction
				}
				
			}
		});
    }
}
