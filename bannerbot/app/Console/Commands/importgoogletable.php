<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\GoogleDocs;
use App\BannerSizes;
use App\BannersSettings;
use App\ExportSettings;
use App\DataSource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;


class importgoogletable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:google';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from Google Docs Spreadsheet';

    protected $prefix;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->prefix = config("storage.prefix");

        parent::__construct();
    }

    private $ds = false;	// Data source

    private function initDataSource() {
        if ($this->ds === false) {
            $this->ds = new DataSource;
        }
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$GoogleData = new GoogleDocs(config('googledocs.SPREADSHEET_ID_IMPORT'));
		$sizes = BannerSizes::get()['items'];
		$structure = BannersSettings::get()['items'];
		$importFields = ExportSettings::getFields();
		$publishByActive = false;

		DB::transaction(function () use (&$GoogleData, &$sizes, &$structure, &$importFields, &$publishByActive) {
			foreach ($sizes as $size) {
				$table_name = $this->prefix.$size['ID'];
				if (Schema::hasTable($table_name)) {
					// download data from Google Docs
					$full_import_data = $GoogleData->get($size['ID']);
					$db_data = [];
					foreach ($full_import_data as $import_data) {
						foreach ($structure[$size['ID']] as $field) {
							$import_field_name = $field['field'];
							$field_name = $this->prefix.$import_field_name;
							if ($field['type'] != 'frames' && !in_array($import_field_name, $importFields[$size['ID']])) {
								// field is not in export/import list, no need to process it
							} else if ($field['type'] != 'frames' && !Schema::hasColumn($table_name, $field_name)) {
								// column is not exists
								// TODO IMPORTANT !!!Drop error!!!
							} else {
								switch ($field['type']) {
									case 'id':
										$db_data[$field_name] = (int)$import_data[$import_field_name];
										if ($db_data[$field_name] < 1) {
											// TODO error
											var_dump("TODO", "ID zero or less", $import_data[$import_field_name], $import_data);
										}
										break;
									case 'boolean':
										// boolean value
										$db_data[$field_name] = $import_data[$import_field_name]=="TRUE"?true:false;
										break;
									case 'active':
										// boolean value
										$publishByActive = true;	// banner set have `active` field, that is mean it should be deployed
										$db_data[$field_name] = $import_data[$import_field_name]=="TRUE"?true:false;
										break;
									case 'ad_id':
										$db_data[$field_name] = (int)$import_data[$import_field_name];
										break;
									case 'frames':
										// handle frames
										for ($frame = 1; $frame <= $size['frames']; $frame++) {
											// and frame fields
											foreach ($field['fields'] as $frame_field) {
												$frame_import_field_name = "Frame".$frame."_".$frame_field['field']; 
												$frame_field_name = $this->prefix.$frame_import_field_name;
												if (!in_array($frame_import_field_name, $importFields[$size['ID']])) {
													// field is not in export/import list, no need to add it
												} else if (!Schema::hasColumn($table_name, $frame_field_name)) {
													// column is not exists
													// TODO IMPORTANT !!!Drop error!!!
												} else {
													// create fields by type
													switch ($frame_field['type']) {
														case 'boolean':
															$db_data[$frame_field_name] = $import_data[$frame_import_field_name]=="TRUE"?true:false;
															break;
														default:
															$db_data[$frame_field_name] = $import_data[$frame_import_field_name];
													}
												}
											}
										}
										break;
									default:
										$db_data[$field_name] = $import_data[$import_field_name];
								}
							}
						}
						// TODO made it more intelligent
						$item_id = DB::table($table_name)->select("DATA_ID")->where("DATA_ID", $db_data['DATA_ID'])->first();
						if ($item_id === null) {
							$db_data['created_at'] = date("Y-m-d H:i:s");
							DB::table($table_name)->insert($db_data);
						} else {
							$item_id = $item_id->DATA_ID;
							$db_data['updated_at'] = date("Y-m-d H:i:s");
							DB::table($table_name)->where("DATA_ID", $item_id)->update($db_data);
						}
					}
				}
			}
		});
		
		if ($publishByActive) {
			$this->publishActiveBanners();
		}
    }

    private function publishActiveBanners() {
		$sizes = BannerSizes::get()['items'];
		$this->initDataSource();
		foreach ($sizes as $size) {
			$bannerSize = $size['ID'];
			$activeField = BannersSettings::getActiveField($bannerSize);
			if ($activeField) {
				$items = \DB::table($this->prefix.$bannerSize)
					->select(['DATA_ID as ID'])
					->where($this->prefix.$activeField, 1)    // get all published
					->whereNull('deleted_at')           // and not removed
					->get()->toArray();                 // banners
				foreach ($items as $item) {
					$this->ds->publishBanner($bannerSize, (int)$item->ID, 1);
				}
			}
		}
	}
}
