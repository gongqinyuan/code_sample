<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerBotACL extends Model
{
	static $methods_settings = [
		"auth" => [
			"active" => false,
		],
		"logout" => [
			"active" => false,
		],
		"cats" => [
			"active" => false,
		],
		"bannersizes" => [
			"active" => false,
		],
		"banners" => [
			"active" => false,
		],
		"deleteBanners" => [
			"active" => false,
		],
		"uploadImage" => [
			"active" => false,
		],
		"overview" => [
			"active" => false,
		],
		"download" => [
			"active" => false,
		],
		"acl" => [
			"active" => false,
		],
		"overviewSettings" => [
			"active" => false,
		],
		"bannersSettings" => [
			"active" => false,
		],
	];

    static function get($user_id) {
		$user_id = (int)$user_id;
		$r = [];
		$acls = \DB::table("users")
			->select("access", \DB::raw('max(init) AS init'), \DB::raw('max(r) AS r'), \DB::raw('max(w) AS w'), \DB::raw('max(x) AS x'))
			->leftJoin("users_groups", "users_groups.user_id", "=", "users.id")
			->leftJoin("groups", "groups.id", "=", "users_groups.group_id")
			->leftJoin("groups_accesses", "groups_accesses.group_id", "=", "groups.id")
			->where("users.id", $user_id)
			->where("groups.is_active", 1)
			->groupBy("access")
			->get();
		foreach ($acls as $acl) {
			if ($acl->init != "") {
				$r['_init_'] = $acl->init;
			}
			$r[$acl->access] = [
				"r" => $acl->r == 1?true:false,
				"w" => $acl->w == 1?true:false,
				"x" => $acl->x == 1?true:false
			];
		}
		return $r;
	}
	
	static function check($user_id, $access_to, $method = 'GET') {
		$user_id = (int)$user_id;
		if (isset(BannerBotACL::$methods_settings[$access_to]) && BannerBotACL::$methods_settings[$access_to]['active'] && $user_id > 0) {
			$access = \DB::table("users")
				->select("access")
				->leftJoin("users_groups", "users_groups.user_id", "=", "users.id")
				->leftJoin("groups", "groups.id", "=", "users_groups.group_id")
				->leftJoin("groups_accesses", "groups_accesses.group_id", "=", "groups.id")
				->where("users.id", $user_id)
				->where("access", $access_to)
				->where((strtoupper($method)=='POST'?'w':'r'), 1)	// POST for `(w)rite`, any other (only GET expected) `(r)ead`
				->where("groups.is_active", 1)
				->count();	// count number of active groups records with r/w access to $access_to for user $user_id
			if ($access > 0) {
				return true;
			}
		}
		return false;
	}
}
