<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Settings;

class BannerSizes extends Model
{
	static function get() {
		return Settings::get('bannersizes');
	}
	
	static function getBySize ($bannerSize) {
		$bannerSizes = BannerSizes::get()['items'];
		foreach ($bannerSizes as $item) {
			if ($item['ID'] == $bannerSize) {
				return $item;
			}
		}
		return false;
	}

	static function validate ($bannerSize) {
		if (BannerSizes::getBySize($bannerSize)) {
			return true;
		}
		return false;
	}
	
}
