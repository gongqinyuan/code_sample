<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Settings;

class Categories extends Model
{
	private $ds = false;	// Data source
	
	private function initDataSource() {
		if ($this->ds === false) {
			$this->ds = new DataSource;
		}
	}

	static function get() {
		return Settings::get('cats');
	}
	
	function getCategoryItems($category, $size) {
		$this->initDataSource();
		return $this->ds->categoryItems($category, $size);
	}
}
