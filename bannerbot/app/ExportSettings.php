<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Settings;
use App\BannersSettings;
use App\BannerSizes;

class ExportSettings extends Model
{
	static function get() {
		return Settings::get('exportSettings');
	}
	
	static function getFields($prefix = false) {
		$exportFields = ExportSettings::get()['items'];
		foreach ($exportFields as $banner_size => $fields) {
			foreach ($fields as $k => $v) {
				$fields[$k] = ($prefix?:'').$v['field'];
			}
			$exportFields[$banner_size] = $fields;
		}
		return $exportFields;
	}
	
	static function getFieldsDB($bannerSize, $prefix) {
		$exportFields = ExportSettings::get()['items'];
		$fields = [];
		foreach ($exportFields[$bannerSize] as $k => $v) {
			$fields[$k] = $prefix.$v['field']." AS ".$v['field'];
		}
		return $fields;
	}
	
	static function getFieldsWithType($bannerSize) {
		$fields = [];
		$structure = BannersSettings::get()['items'][$bannerSize];
		$size = BannerSizes::getBySize($bannerSize);
		foreach ($structure as $field) {
			$field_name = $field['field'];
			$field_type = $field['type'];
			
			if ($field_type == 'frames') {
				for ($frame = 1; $frame <= $size['frames']; $frame++) {
					foreach ($field['fields'] as $frame_field) {
						$frame_field_name = $frame_field['field'];
						$frame_field_name_flat = "Frame".$frame."_".$frame_field_name; 
						$frame_field_type = $frame_field['type'];
						$fields[$frame_field_name_flat] = $frame_field_type;
					}
				}
			} else {
				$fields[$field_name] = $field_type;
			}
		}

		return $fields;
	}
}
