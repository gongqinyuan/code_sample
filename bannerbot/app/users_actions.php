<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users_actions extends Model
{
	private static $log_events = ['banner.store', 'banner.destroy', 'banner.update', 'overview.update', 'pubish.store', 'pubish.destroy', 'upload.store'];
	
	public static function log($method, $route, $data, $result, $user_id = false) {
		if (in_array($route, self::$log_events)) {
			$log = new users_actions();
			$log->user_id = $user_id?:\Auth::user()->id;
			$log->method = $route;
			$log->data = json_encode($data);
			$log->result = $result;
			if ($log->save()) {
				return true;
			}
			return false;
		}
		return true;
	}
}
