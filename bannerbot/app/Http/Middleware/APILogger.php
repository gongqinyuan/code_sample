<?php
namespace App\Http\Middleware;

use Closure;
use App\users_actions;

class APILogger 
{
	public function handle($request, Closure $next) {
		return $next($request);
	}

	public function terminate($request, $response) {
		users_actions::log(
			$request->method(), 
			$request->route()->getName(), 
			$request->all(), 
			$response->getContent()
		);
	}
}