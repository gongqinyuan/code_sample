<?php

namespace App\Http\Middleware;

use Closure;

class IpMiddleware
{
    /**
     * Handle an incoming request and reject anyone not in whitelist of IP addresses.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if ($request->ip() 
			&& is_array(config('auth.ip_whitelist')) 
			&& in_array($request->ip(), config('auth.ip_whitelist'))
		) {
			return $next($request);
        }
        return redirect('login');
    }
}
