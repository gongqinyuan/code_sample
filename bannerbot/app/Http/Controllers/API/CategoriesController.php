<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIController as APIController;
use App\Categories;

class CategoriesController extends APIController {
	// Return categories
	
	public function index() {
		return $this->success(Categories::get());
	}
	
	public function show($category, $size) {
		$result = (new Categories())->getCategoryItems($category, $size);
		if ($result !== false) {
			return $this->success($result);
		} else {
			return $this->error("");
		}
	}
}