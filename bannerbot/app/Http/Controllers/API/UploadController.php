<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIController as APIController;
use App\Tinify;

class UploadController extends APIController {
	// Upload image to tinify and AWS
	
	public function store(Request $request) {
		if ($request->file('img')) {
			$img = $request->file('img')->getPathName();
			if ($result = Tinify::upload($img)) {
				return $this->success($result);
			} else {
				return $this->error("upload_error");
			}
		} else {
			return $this->error("no_file");
		}
	}
}