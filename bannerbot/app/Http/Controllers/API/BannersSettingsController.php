<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIController as APIController;
use App\BannersSettings;

class BannersSettingsController extends APIController {
	// Return banners settings
	
	public function index() {
		$user = \Auth::user();
		$data = BannersSettings::get($user->id);
		if ($data) {
			return $this->success($data);
		} else {
			return $this->error_access("BannersSettings");
		}
	}
}