<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIController as APIController;
use App\OverviewSettings;

class OverviewSettingsController extends APIController {
	// Return overview page settings
	
	public function index() {
		$user = \Auth::user();
		$data = OverviewSettings::get($user->id);
		if ($data) {
			return $this->success($data);
		} else {
			return $this->error_access("OverviewSettings");
		}
	}
}