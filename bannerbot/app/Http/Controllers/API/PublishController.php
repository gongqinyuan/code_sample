<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIController as APIController;
use App\DataSource;

class PublishController extends APIController {
	// Publish methods
	
	private $ds = false;	// Data source
	
	private function initDataSource() {
		if ($this->ds === false) {
			$this->ds = new DataSource;
		}
	}
	
	public function store($bannerSize, $ID) {
		$this->initDataSource();
		$result = $this->ds->publishBanner($bannerSize, $ID);
		if ($result !== false) {
			return $this->success(['ID' => $result]);
		} else {
			return $this->error($this->ds->getError());
		}
	}
	
	public function destroy($bannerSize, $ID) {
		$this->initDataSource();
		$result = $this->ds->unpublishBanner($bannerSize, $ID);
		if ($result !== false) {
			return $this->success(['ID' => $result]);
		} else {
			return $this->error($this->ds->getError());
		}
	}
	
}