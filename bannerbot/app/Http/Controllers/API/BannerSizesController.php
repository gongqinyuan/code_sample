<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIController as APIController;
use App\BannerSizes;

class BannerSizesController extends APIController {
	// Return categories
	
	public function index() {
		return $this->success(BannerSizes::get());
	}
}