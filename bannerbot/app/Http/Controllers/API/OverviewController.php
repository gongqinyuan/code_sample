<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIController as APIController;
use App\DataSource;

class OverviewController extends APIController {
	// Return overview data
	
	private $ds = false;	// Data source
	
	private function initDataSource() {
		if ($this->ds === false) {
			$this->ds = new DataSource;
		}
	}
	
	public function show($bannerSize) {
		$this->initDataSource();
		$data = $this->ds->getOverview($bannerSize);
		if ($data !== false) {
			return $this->success($data);
		} else {
			return $this->error($this->ds->getError());
		}
	}
	
	public function update(Request $request, $bannerSize) {
		$this->initDataSource();
		$result = $this->ds->setOverview($bannerSize, $request->input('data'));
		if ($result !== false) {
			return $this->success($result);
		} else {
			return $this->error($this->ds->getError());
		}
	}
}