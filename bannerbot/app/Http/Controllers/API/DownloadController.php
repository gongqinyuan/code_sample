<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIController as APIController;

class DownloadController extends APIController {
	// Download backup image
	
	public function index(Request $request) {
		if ($request->input('url') && substr($request->input('url'),0,4) == 'http' && strstr($request->input('url'),'bbanner') && $request->input('filename')) {
			$file_content = file_get_contents($request->input('url'));	// WARNING file should be much smaller than PHP memory limit
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.$request->input('filename').'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . strlen($file_content));
			flush(); // Flush system output buffer
			print $file_content;
			exit;
		} else {
			print "Wrong URL";
		}
		die();
	}
}