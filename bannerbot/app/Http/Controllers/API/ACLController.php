<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIController as APIController;
use App\BannerBotACL;

class ACLController extends APIController {
	// Return access control data
	
	public function index() {
		$user = \Auth::user();
		return $this->success(BannerBotACL::get($user->id));
	}
}