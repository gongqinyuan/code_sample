<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class APIController extends Controller {
	// API JSON responses

	public function success($result, $code = 200, $raw = false) {
		if ($raw) {
			$response = $result;
		} else {
			$response = [
				'success' => true,
				'message' => $result,
			];
		}
		return response()->json($response, $code)
			->header('Cache-Control', 'no-store,no-cache, must-revalidate, post-check=0, pre-check=0');
	}

	public function error($error, $code = 404, $raw = false) {
		if ($raw) {
			$response = $error;
		} else {
			$response = [
				'success' => false,
				'message' => $error,
			];
		}
		return response()->json($response, $code)
			->header('Cache-Control', 'no-store,no-cache, must-revalidate, post-check=0, pre-check=0');
	}
	
	public function error_access($error) {
		return $this->error($error, 401);
	}

}