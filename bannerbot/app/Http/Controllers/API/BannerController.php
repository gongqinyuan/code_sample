<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIController as APIController;
use App\DataSource;

class BannerController extends APIController {
	// Return banner data
	
	private $ds = false;	// Data source
	
	private function initDataSource() {
		if ($this->ds === false) {
			$this->ds = new DataSource;
		}
	}
	
	public function destroy($bannerSize, $ID) {
		$this->initDataSource();
		$result = $this->ds->deleteBanner($bannerSize, $ID);
		if ($result !== false) {
			return $this->success(['deleted' => $result]);
		} else {
			return $this->error($this->ds->getError());
		}
	}
	
	public function update(Request $request, $bannerSize, $ID) {
		$this->initDataSource();
		$result = $this->ds->updateBanner($bannerSize, $ID, $request->input('data'));
		if ($result !== false) {
			return $this->success(['ID' => $result]);
		} else {
			return $this->error($this->ds->getError());
		}
	}
	

	
	public function store(Request $request, $bannerSize) {
		// create new empty banner
		$this->initDataSource();
		$result = $this->ds->addNew($request->input('category'), $bannerSize, $request->input('bannerName'), $request->input('adID'));
		if ($result !== false) {
			return $this->success(['ID' => $result]);
		} else {
			return $this->error($this->ds->getError());
		}		
	}
	
	public function versions($bannerSize, $ID) {
		$this->initDataSource();
		$result = $this->ds->bannerVersions($bannerSize, $ID);
		if ($result !== false) {
			return $this->success($result);
		} else {
			return $this->error($this->ds->getError());
		}
	}
	
	public function show($bannerSize, $ID, $versionID) {
		$this->initDataSource();
		$data = $this->ds->getBanner($bannerSize, $ID, $versionID);
		if ($data !== false) {
			return $this->success($data);
		} else {
			return $this->error($this->ds->getError());
		}
	}
	
}