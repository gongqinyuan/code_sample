<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\BannerSizes;
use App\BannerItem;
use App\OverviewSettings;
use App\BannersSettings;
use App\users_actions;
use App\SyncTask;

class DataSource extends Model
{
	protected $prefix;
	protected $postfix_history;
	protected $postfix_published;
	
	private $error = false;

	public function __construct() {
		$this->prefix = config("storage.prefix");
		$this->postfix_history = config("storage.postfix_history");
		$this->postfix_published = config("storage.postfix_published");
	}

	private function dt() {
		return date("Y-m-d H:i:s");
	}
	
	private function getUserId() {
		return \Auth::user()->id?:0;
	}
	
	private function setError($error) {
		$this->error = $error;
	}
	public function getError() {
		return $this->error;
	}
	
	// History
	
	private function setHistory($bannerSize, $ID, $user_id = false) {
		// get all data fields "as is" with some extra fields
		$fields = ExportSettings::getFields()[$bannerSize];
		foreach ($fields as &$field) {
			$field = $this->prefix.$field;	// Hardcoded prefix for DB items
		}
		$fields[] = "user_id";
		$fields[] = "published";
		$fields[] = "created_at";	// copy timestamps fieds also
		$fields[] = "updated_at";
		$fields[] = "deleted_at";
		$item = \DB::table($this->prefix.$bannerSize)
			->select($fields)
			->where($this->prefix."ID", $ID)
			->first();
		$item = json_decode(json_encode($item), true);
		
		// add current user_id to know who is update/delete this item
		$item['history_user_id'] = $user_id?:$this->getUserId();
		$item['history_at'] = $this->dt();
		
		try {
			\DB::table($this->prefix.$bannerSize.$this->postfix_history)
				->insertGetId($item);
			return true;
		} catch (\Exception $e) {
			Log::error("setHistory DB exception", 
			[ 
				'error' => $e->getMessage(),
				'size' => $bannerSize, 
				'ID' => $ID
			]);
			$this->setError("DB");
			return false;
		}
	}
	

	
	// Overview
	
	function getOverview($bannerSize) {
		if (BannerSizes::validate($bannerSize)) {
			$fields = OverviewSettings::getFieldsDB($this->prefix);
			$fields[] = $this->prefix."Backup_Image_URL AS Backup_Image_URL";
			$fields[] = $this->prefix."ID AS ID";
			$items = \DB::table($this->prefix.$bannerSize)
				->select($fields)
				->whereNull('deleted_at')
				->get()->toArray();
			$items = BannerItem::processData($bannerSize, $items, true);
			
			// Published state
			foreach ($items as &$item) {
				$published = $this->getPublishedBanner($bannerSize, $item['ID']);
				$item['_published'] = $published?true:false;
			}

			return $items;
		} else {
			Log::error("getOverview validation", 
			[ 
				'size' => $bannerSize
			]);
			$this->setError("invalid_data");
		}
		return false;
	}
	
	function setOverview($bannerSize, $data) {
		$data = BannerItem::processData($bannerSize, $data, true);
		if ($data && BannerSizes::validate($bannerSize)) {
			// if banner published, all updates should be moved to published version also and deploy to Google Docs
			$is_published = [];
			// create data diff, we don't need to INSERT, as it is UPDATE only method
			$to_update = [];
			// diff based on current data in DB
			$current_data = $this->getOverview($bannerSize);
			foreach ($current_data as $item) {
				if (array_key_exists($item['ID'], $data)) {
					$is_published[$item['ID']] = ($this->getPublishedBanner($bannerSize, $item['ID']))?true:false;
					// we found item
					foreach ($data[$item['ID']] as $field => $value) {
						if ($value != $item[$field]) {
							// $field of current $item has been updated
							if (!isset($to_update[$item['ID']])) $to_update[$item['ID']] = [];
							$to_update[$item['ID']][$this->prefix.$field] = $value;
						}
					}
				}
			}
			if (count($to_update) > 0) {
				try {
					\DB::transaction(function() use (&$bannerSize, &$to_update, &$is_published) {
						foreach($to_update as $item_id => $item) {
							$this->setHistory($bannerSize, $item_id);
							$item['user_id'] = $this->getUserId();
							\DB::table($this->prefix.$bannerSize)
								->where($this->prefix."ID", $item_id)
								->whereNull('deleted_at')
								->update($item);
							if ($is_published[$item_id]) {
								// update published version of banner
								\DB::table($this->prefix.$bannerSize.$this->postfix_published)
									->where($this->prefix."ID", $item_id)
									->update($item);
								// mark do deploy
								SyncTask::add($bannerSize);
							}
						}
					});
				} catch ( \Exception $e) {
					Log::error("setOverview DB exception", 
					[ 
						'error' => $e->getMessage(),
						'size' => $bannerSize, 
						'update' => $to_update,
						'data' => $data
					]);
					$this->setError("DB");
					return false;
				}
			}
			return true;
		} else {
			Log::error("setOverview validation", 
			[ 
				'size' => $bannerSize, 
				'data' => $data
			]);
			$this->setError("invalid_data");
		}
		return false;
	}
	

	private function getPublishedBanner($bannerSize, $ID) {
		$fields = ExportSettings::getFieldsDB($bannerSize, $this->prefix);
		return \DB::table($this->prefix.$bannerSize.$this->postfix_published)
			->select($fields)
			->where($this->prefix."ID", $ID)
			->first();
	}


	// Unpublish and delete banner
	function deleteBanner($bannerSize, $ID) {
		if (BannerSizes::validate($bannerSize)) {
			$this->setHistory($bannerSize, $ID);
			try {
				$this->unpublishBanner($bannerSize, $ID);
				\DB::table($this->prefix.$bannerSize)
					->where($this->prefix."ID", $ID)
					->update([ 'deleted_at' => $this->dt() ]);
				return true;
			} catch (\Exception $e) {
				Log::error("deleteBanner DB exception", 
				[ 
					'error' => $e->getMessage(),
					'size' => $bannerSize, 
					'ID' => $ID
				]);
				$this->setError("DB");
				return false;
			}
		} else {
			Log::error("deleteBanner validation", 
			[ 
				'size' => $bannerSize, 
				'ID' => $ID
			]);
			$this->setError("invalid_data");
		}
		return false;
	}


	// Banner unpublish
	function unpublishBanner($bannerSize, $ID) {
		if (BannerSizes::validate($bannerSize)) {
			try {
				\DB::table($this->prefix.$bannerSize.$this->postfix_published)
					->where($this->prefix."ID", $ID)
					->delete();
				// after remove from publish table
				SyncTask::add($bannerSize);
				return $ID;
			} catch (\Exception $e) {
				Log::error("unpublishBanner DB exception", 
				[ 
					'error' => $e->getMessage(),
					'size' => $bannerSize, 
					'ID' => $ID
				]);
				$this->setError("DB");
				return false;
			}
		} else {
			Log::error("unpublishBanner validation", 
			[ 
				'size' => $bannerSize, 
				'ID' => $ID
			]);
			$this->setError("invalid_data");
		}
		return false;
	}
	
	// Banner publish
	function publishBanner($bannerSize, $ID, $user_id = false) {
		$fields = ExportSettings::getFields()[$bannerSize];
		foreach ($fields as &$field) {
			$field = $this->prefix.$field;	// Hardcoded prefix for DB items
		}
		$fields[] = "user_id";
		$item = \DB::table($this->prefix.$bannerSize)
			->select($fields)
			->where($this->prefix."ID", $ID)
			->whereNull('deleted_at')
			->first();
		$check_field_AdID            = $this->prefix.BannersSettings::getAdIdField($bannerSize);
		$check_field_BannerName      = $this->prefix.BannersSettings::getBannerNameField($bannerSize);
		$check_field_ClickthroughUrl = $this->prefix.BannersSettings::getClickthroughUrlField($bannerSize);
		if ($item
			&& isset($item->$check_field_AdID)            && $item->$check_field_AdID
			&& isset($item->$check_field_BannerName)      && $item->$check_field_BannerName
			&& isset($item->$check_field_ClickthroughUrl) && $item->$check_field_ClickthroughUrl
		) {
			$item = json_decode(json_encode($item), true);
			$activeField = BannersSettings::getActiveField($bannerSize);
			if ($activeField) $item[$this->prefix.$activeField] = 1; // Activate banner on publish
			try {
				\DB::transaction(function() use (&$bannerSize, &$item, &$user_id) {
					// put item data into published table
					\DB::table($this->prefix.$bannerSize.$this->postfix_published)
						->updateOrInsert(
							[$this->prefix."ID" => $item[$this->prefix."ID"]],
							array_merge(
								$item, 
								[
									'publish_user_id' => $user_id?:$this->getUserId()
								]
							)
						);
					// update current table with new published status
					\DB::table($this->prefix.$bannerSize)
						->where($this->prefix."ID", $item[$this->prefix."ID"])
						->whereNull('deleted_at')
						->update(['published' => 1]);
				});
				// Add update into sync tasks queue
				SyncTask::add($bannerSize);
				return $item[$this->prefix."ID"];
			} catch (\Exception $e) {
				Log::error("publishBanner DB exception", 
				[ 
					'error' => $e->getMessage(),
					'user_id' => $user_id,
					'size' => $bannerSize, 
					'ID' => $ID, 
					'item' => $item
				]);
				$this->setError("DB");
				return false;
			}
		} else {
			Log::error("publishBanner validation", 
			[ 
				'size' => $bannerSize, 
				'user_id' => $user_id,
				'ID' => $ID, 
				'item' => $item,
				'check_fields' => [
					'AdID' => $check_field_AdID, 
					'BannerName' => $check_field_BannerName, 
					'ClickthroughUrl' => $check_field_ClickthroughUrl
				]
			]);
			$this->setError("invalid_data");
		}
		return false;
	}
	
	// save banner
	function updateBanner($bannerSize, $ID, $item) {
		$ID = (int)$ID;
		if (BannerSizes::validate($bannerSize) && $ID > 0) {
			$item = BannerItem::processItem($bannerSize, $item, false);
			$tmp = [];
			foreach ($item as $k => $v) {
				$tmp[$this->prefix.$k] = $v;
			}
			$item = $tmp;
			unset($tmp);
			unset($item['DATA_ID']);
			$item['user_id'] = $this->getUserId();
			$this->setHistory($bannerSize, $ID);
			$item['updated_at'] = $this->dt();
			try {
				\DB::table($this->prefix.$bannerSize)
					->where($this->prefix."ID", $ID)
					->whereNull('deleted_at')
					->update($item);
			} catch (\Exception $e) {
				Log::error("updateBanner DB exception", 
				[ 
					'error' => $e->getMessage(),
					'size' => $bannerSize, 
					'ID' => $ID, 
					'item' => $item
				]);
				$this->setError("DB");
				return false;
			}
			return $ID;
		} else {
			Log::error("updateBanner validation", 
			[ 
				'category' => $category, 
				'size' => $bannerSize, 
				'ID' => $ID, 
				'item' => $item
			]);
			$this->setError("invalid_data");
		}
		return false;
	}
	
	// add new empty banner
	function addNew($category, $bannerSize, $bannerName, $bannerAdId = 0) {
		// create new empty banner with name (and Ad/Placement ID when available)
		$category_field = BannersSettings::getIdField($bannerSize);
		$name_field = BannersSettings::getBannerNameField($bannerSize);
		$adId_field = BannersSettings::getAdIdField($bannerSize);
		if (BannerSizes::validate($bannerSize)
			&& $category_field
			&& $name_field
			&& $adId_field
		) {
			try {
				return \DB::table($this->prefix.$bannerSize)
					->insertGetId([
						$this->prefix.$category_field => $category,
						$this->prefix.$name_field     => $bannerName,
						$this->prefix.$adId_field     => (int)$bannerAdId>0?:0
					]);
				
			} catch (\Exception $e) {
				Log::error("addNew DB exception", 
				[ 
					'error' => $e->getMessage(),
					'category' => $category, 
					'size' => $bannerSize, 
					'name' => $bannerName, 
					'adId' => $bannerAdId, 
					'category_field' => $category_field, 
					'name_field' => $name_field, 
					'adId_field' => $adId_field 
				]);
				$this->setError("DB");
				return false;
			}
		} else {
			Log::error("addNew validation", 
			[ 
				'category' => $category, 
				'size' => $bannerSize, 
				'name' => $bannerName, 
				'adId' => $bannerAdId, 
				'category_field' => $category_field, 
				'name_field' => $name_field, 
				'adId_field' => $adId_field 
			]);
			$this->setError("invalid_data");
		}
		return false;
	}
	
	// Get banners category
	function categoryItems ($category, $bannerSize) {
		// return list of items in category
		$category_field = BannersSettings::getIdField($bannerSize);
		$name_field = BannersSettings::getBannerNameField($bannerSize);
		if (BannerSizes::validate($bannerSize)
			&& $category_field
			&& $name_field
		) {
			try {
				$result = \DB::table($this->prefix.$bannerSize)
					->select([ $this->prefix.'ID AS ID', $this->prefix.$name_field.' AS title' ])
					->where($this->prefix.$category_field, $category)
					->whereNull('deleted_at')
					->orderBy($this->prefix.$name_field)
					->get();
				$out = [];
				foreach ($result as $item) {
					$out[$item->ID] = [
						'ID' => $item->ID, 
						'title' => $item->title 
					];
				}
				return $out;
			} catch (\Exception $e) {
				Log::error("categoryItems DB exception", 
				[ 
					'error' => $e->getMessage(),
					'category' => $category, 
					'size' => $bannerSize, 
					'category_field' => $category_field, 
					'name_field' => $name_field 
				]);
				$this->setError("DB");
				return false;
			}

		} else {
			Log::error("categoryItems validation", 
			[ 
				'category' => $category, 
				'size' => $bannerSize, 
				'category_field' => $category_field, 
				'name_field' => $name_field 
			]);
			$this->setError("invalid_data");
		}
		return false;
	}
	
	// Get banners version
	function bannerVersions ($bannerSize, $ID) {
		if (BannerSizes::validate($bannerSize)) {
			// This is hardcoded versions, as at that iteration we need only draft and published versions
			// I'm feel that real versioning will be in future
			$is_published = ($this->getPublishedBanner($bannerSize, $ID))?true:false;
			if ($is_published) {
				return [
					1 => ['ID' => 1, 'title' => 'Version: Draft'],
					2 => ['ID' => 2, 'title' => 'Version: Published']
				];
			} else {
				return [
					1 => ['ID' => 1, 'title' => 'Version: Draft']
				];
			}
		}
		return false;
	}
	
	// Get banner data
	function getBanner($bannerSize, $ID, $versionID) {
		if (BannerSizes::validate($bannerSize)) {
			$fields = ExportSettings::getFieldsDB($bannerSize, $this->prefix);
			try {
				$result = \DB::table($this->prefix.$bannerSize.($versionID==2?$this->postfix_published:''))	// versionID = 2 is published banner
					->select($fields)
					->where($this->prefix."ID", $ID);
				if ($versionID == 2) {
				} else {
					$result = $result->whereNull('deleted_at');
				}
				$result = $result->first();
				return BannerItem::processItem($bannerSize, json_decode(json_encode($result), true), false);
			} catch (\Exception $e) {
				Log::error("getBanner DB exception", 
				[ 
					'error' => $e->getMessage(),
					'size' => $bannerSize, 
					'ID' => $ID, 
					'versionID' => $versionID 
				]);
				$this->setError("DB");
				return false;
			}
		} else {
			Log::error("getBanner validation", 
			[ 
				'size' => $bannerSize, 
				'ID' => $ID, 
				'versionID' => $versionID 
			]);
			$this->setError("invalid_data");
		}
		return false;
	}

}
