<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\GoogleDocs;
use App\Settings;
use App\ExportSettings;

class SyncTask extends Model
{
    protected $attributes = [
        'result' => "",
    ];

    protected static $prefix;
    protected static $postfix_published;

    public function __construct()
    {
        self::$prefix = config("storage.prefix");
        self::$postfix_published = config("storage.postfix_published");
    }

    public static function add($bannerSize) {
		$task = new SyncTask;
		$task->bannerSize = $bannerSize;
		if ($task->save()) {
			return true;
		}
		return false;
	}
	
	public static function run() {
		// TODO, use `hrtime` instead of `microtime` for PHP 7.3+
		
		$limit = 59;	// task should be active up to 1 minute
		$check_delta = 3;	// check updates every 3 seconds, in total it will be 57 seconds until limit
		while ($limit > 0) {
			$startTS = microtime(true);
			
			self::runTasks();
			
			$delta = microtime(true) - $startTS;
			if ($delta < $check_delta) {
				// done less than in `check_delta` seconds, so need to wait a little bit if limit is not expired
				if ($delta < 0) $delta = 0;	// delta is wrong
				$limit = $limit - $check_delta;
				if ($limit > 0) {
					usleep(($check_delta - $delta)*1000000);
					$delta = 0;
				}
			} else {
				$limit = $limit - $delta;
			}
		}
	}
	
	private static function runTasks() {
		// update lastSync settings
		$start_at = date("Y-m-d H:i:s");
		// clean all tasks that are in progress since last time
		self::where('progress', 'run')->update(['progress' => 'wait']);
		// retry failed tasks
		self::where('progress', 'finished')->where('success', 0)->where('retry', '>', 0)->decrement('retry', 1, ['progress' => 'wait']);
		// get all tasks that are not in progress
		$tasks = self::where('progress', 'wait')->orderBy('created_at', 'desc')->get();
		// group by tables
		$tables = [];
		foreach ($tasks as $task) {
			if (!isset($tables[$task->bannerSize])) {
				$tables[$task->bannerSize] = [];
			}
			$tables[$task->bannerSize][] = $task;
		}
		unset($tasks);
		// run tasks
		foreach ($tables as $bannerSize => $tasks) {
			$result = self::runTask($bannerSize);
			// set task status
			$first_task = false;
			foreach ($tasks as $task) {
				$task->success = $result['success']?1:0;
				if ($first_task === false) {
					// this is the first task in group
					$task->progress = 'finished';
					$task->result = json_encode($result);
					$first_task = $task->id;
				} else {
					// all other tasks finished by first task
					$task->progress = 'finished_by_parent';
					$task->result = json_encode(["parent_id" => $first_task]);
				}
				$task->save();
			}
		}
		Settings::set('lastSync', [ "start_at" => $start_at, "finished_at" => date("Y-m-d H:i:s") ]);
	}
	
	private static function runTask($bannerSize) {
		$out = [
			'success' => false,
			'message' => ""
		];
		
		$GoogleData = new GoogleDocs(config('googledocs.SPREADSHEET_ID_EXPORT'));
		$export_fields = ExportSettings::getFields()[$bannerSize];
		
		$fields = [];
		foreach ($export_fields as $field) {
			$fields[] = self::$prefix.$field." AS ".$field;
		}
		
		$to_publish = \DB::table(self::$prefix.$bannerSize.self::$postfix_published)
			->select($fields)
			->orderBy("DATA_ID")
			->get()->toArray();
			
		$field_types = ExportSettings::getFieldsWithType($bannerSize);
		
		$to_publish = json_decode(json_encode($to_publish), true);
		
		// Convert boolean values to TRUE/FALSE (important for import only)
		foreach ($to_publish as $i => &$item) {
			foreach ($item as $k => &$v) {
				if (isset($field_types[$k])) {
					switch ($field_types[$k]) {
						case 'boolean':
							Log::debug("SyncTask convert", ["key" => $k, "v" => $v, "field"=> $field_types[$k]]);
							$v = $v?'TRUE':'FALSE';
							break;
						case 'active':
							Log::debug("SyncTask convert", ["key" => $k, "v" => $v, "field"=> $field_types[$k]]);
							$v = $v?'TRUE':'FALSE';
							break;
						//default:
						//	Log::debug("SyncTask nothing to convert", ["key" => $k, "v" => $v, "field"=> $field_types[$k]]);
					}
				} else {
					Log::error("SyncTask", ["key" => $k, "field"=> isset($field_types[$k])]);
				}
			}
		}
		
		// add header
		array_unshift($to_publish, $export_fields);
		
		if ($GoogleData->set($bannerSize, $to_publish)) {
			$out['success'] = true;
			$out['message'] = ["items" => count($to_publish)-1];
			return $out;
		} else {
			$out['success'] = false;
			return $out;
		}
	}
}
