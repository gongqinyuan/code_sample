<?php

namespace App;
use Illuminate\Support\Facades\Log;
use App\ext_images;

class Tinify
{
	static function upload($img) {
		if ($img) {
			$retry_counter = 3;
			while ($retry_counter >= 0) {
				$response = self::uploadImage($img);
				if ($response !== false) { 
					// success
					$retry_counter = -9; break; 
				}
				$retry_counter--;
			}
			if ($retry_counter == -9) {
				$retry_counter = 3;
				while ($retry_counter >= 0) {
					$out = self::uploadStorage($response);
					if ($out['success'] === true) { 
						// success
						$retry_counter = -9; break; 
					}
					$retry_counter--;
				}
			} else {
				Log::error("Tinify.upload uploadImage error");
			}
			if ($retry_counter == -9) {
				// saved
				// return image URL
				$img = new ext_images();
				$img->user_id = \Auth::user()->id;
				$img->url = $out['message'];
				$img->save();
				return $out['message'];
			} else {
				Log::error("Tinify.upload uploadStorage error");
			}
			@unlink($img);
		}
		return false;
	}
	
	static private function uploadImage($img) {
		$ch = curl_init();
		$f = file_get_contents($img);
		if (substr($f,0,10) == 'data:image') {
			$f = explode(",",$f);
			$f = base64_decode($f[1]);
		}
		curl_setopt($ch, CURLOPT_URL,				"https://api.tinify.com/shrink");
		curl_setopt($ch, CURLOPT_POST,				true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,		$f);
		curl_setopt($ch, CURLOPT_HEADER,			false);
		curl_setopt($ch, CURLOPT_USERPWD,			"api:".config("tinify.API_KEY"));
		curl_setopt($ch, CURLOPT_TIMEOUT,			60);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,	true);
		$response = curl_exec ($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close ($ch);
		if ($status == 201) {
			Log::debug($response);
			return json_decode($response, true);
		} else {
			Log::error($response);
		}
		return false;
	}
	
	static private function uploadStorage($response) {
		// Now save into AWS via Tinify
		$out = [];
		$store = [
			"store" => [
				"service" => "s3",
				"aws_access_key_id" => config("tinify.AWS_KEY"),
				"aws_secret_access_key" => config("tinify.AWS_SECRET"),
				"region" => config("tinify.AWS_REGION"),
				"path" => config("tinify.AWS_BUCKET")."/".date("Ym")."/".time().rand(10000,99999).".".($response['output']['type']=='image/jpeg'?'jpg':'png'),
			]
		];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,				$response['output']['url']);
		curl_setopt($ch, CURLOPT_POST,				true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,		json_encode($store));
		curl_setopt($ch, CURLOPT_HEADER,			false);
		curl_setopt($ch, CURLOPT_USERPWD,			"api:".config("tinify.API_KEY"));
		curl_setopt($ch, CURLOPT_TIMEOUT,			60);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,	true);
		curl_setopt($ch, CURLOPT_HTTPHEADER,		array('Content-Type: application/json'));
		$aws_response = curl_exec ($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close ($ch);
		if ($status == 200) {
			Log::debug($aws_response);
			$out['success'] = true;
			$out['message'] = "https://s3-".config("tinify.AWS_REGION").".amazonaws.com/".$store['store']['path'];
		} else {
			Log::error($aws_response);
			$out['success'] = false;
			$out['message'] = ["error" => $aws_response];
		}
		return $out;
	}

}
