<?php

namespace App;

use App\BannerSizes;
use App\BannersSettings;
use App\ExportSettings;

class BannerItem
{
	
	static private function _number($value) {
		return (int)$value;
	}
	
	static private function _array($value) {
		if (is_string($value)) {
			return str_split($value);
		} else {
			return implode("", $value);
		}
	}
	
	static private function _boolean($value) {
		if (is_bool($value)) {
			return $value===true;
		} else if (is_numeric($value)) {
			return $value===1;
		} else {
			return (strtoupper($value) == "TRUE" || $value === "1");
		}
	}
	
	static private function _string($value) {
		$brs2nl = false;
		$brs = ["<br>", "<br/>", "<br />"];
		$nls = ["\r\n", "\r", "\n"];
		foreach ($brs as $br) {
			if (stristr($value, $br)) {
				$brs2nl = true;
				break;
			}
		}
		if ($brs2nl) {
			return str_replace($brs, "\n", (string)$value);
		} else {
			return str_replace($nls, "<br>", (string)$value);
		}
	}
	
	static function processData($bannerSize, $data, $diff = false) {
		foreach ($data as $id => $item) {
			$data[$id] = self::processItem($bannerSize, $item, $diff);
		}
		return $data;
	}
	
	static function processItem($bannerSize, $item, $diff = false) {
		// this function will process/validate data from source (front-end, Google Docs, etc)
		// and convert all incoming data according to field types
		// "flat" data (from DB, from Google Docs, e.g. field is Frame3_Copy and not Frames->3->Copy) will be converted to "array"
		// "array" data will be converted to "flat"
		// $diff = true will return only passed values
		// $diff = false will return full array of expected values by $structure
		if (is_object($item)) $item = json_decode(json_encode($item), true);
		if ($item && BannerSizes::validate($bannerSize)) {
			$size = BannerSizes::getBySize($bannerSize);
			$structure = BannersSettings::get()['items'][$bannerSize];
			$importFields = ExportSettings::getFields()[$bannerSize];
			
			$formatted_item = [];
			
			foreach ($structure as $field) {
				$field_name = $field['field'];
				$field_type = $field['type'];
				if ($field_type != 'frames' && !in_array($field_name, $importFields)) {
					// field is not in export/import list, no need to process it
				} else {
					switch ($field_type) {
						case 'id':
							if (!$diff || array_key_exists($field_name, $item)) {
								$formatted_item[$field_name] = isset($item[$field_name])?self::_number($item[$field_name]):null;
							}
							break;
						case 'boolean':
							if (!$diff || array_key_exists($field_name, $item)) {
								$formatted_item[$field_name] = isset($item[$field_name])?self::_boolean($item[$field_name]):null;
							}
							break;
						case 'active':
							if (!$diff || array_key_exists($field_name, $item)) {
								$formatted_item[$field_name] = isset($item[$field_name])?self::_boolean($item[$field_name]):null;
							}
							break;
						case 'ad_id':
							if (!$diff || array_key_exists($field_name, $item)) {
								$formatted_item[$field_name] = isset($item[$field_name])?self::_number($item[$field_name]):0;
							}
							break;
						case 'integer':
							if (!$diff || array_key_exists($field_name, $item)) {
								$formatted_item[$field_name] = isset($item[$field_name])?self::_number($item[$field_name]):0;
							}
							break;
						case 'array':
							if (!$diff || array_key_exists($field_name, $item)) {
								$formatted_item[$field_name] = isset($item[$field_name])?self::_array($item[$field_name]):null;
							}
							break;
						case 'frames':
							// handle frames
							$forDB = isset($item['Frames'])&&is_array($item['Frames']);	// if "true" convert array to by frame item
							if ($forDB || !$diff) {
								if (!$forDB) $formatted_item[$field_name] = [];
								for ($frame = 1; $frame <= $size['frames']; $frame++) {
									// and frame fields
									if (!$forDB) $formatted_item[$field_name][$frame-1] = [ "_ID" => $frame ];
									foreach ($field['fields'] as $frame_field) {
										$frame_field_name = $frame_field['field'];
										$frame_field_name_flat = "Frame".$frame."_".$frame_field_name; 
										$frame_field_type = $frame_field['type'];
										if (!in_array($frame_field_name_flat, $importFields)) {
											// field is not in export/import list, no need to add it
										} else {
											// create fields by type
											switch ($frame_field_type) {
												case 'YN':
													if ($forDB) {
														if (!$diff || array_key_exists($frame_field_name, $item[$field_name][$frame-1])) {
															$formatted_item[$frame_field_name_flat] = isset($item[$field_name][$frame-1][$frame_field_name])?self::_array($item[$field_name][$frame-1][$frame_field_name]):null;
														}
													} else {
														if (!$diff || array_key_exists($frame_field_name_flat, $item)) {
															$formatted_item[$field_name][$frame-1][$frame_field_name] = isset($item[$frame_field_name_flat])?self::_array($item[$frame_field_name_flat]):null;
														}
													}
													break;
												case 'integer':
													if ($forDB) {
														if (!$diff || array_key_exists($frame_field_name, $item[$field_name][$frame-1])) {
															$formatted_item[$frame_field_name_flat] = isset($item[$field_name][$frame-1][$frame_field_name])?self::_number($item[$field_name][$frame-1][$frame_field_name]):0;
														}
													} else {
														if (!$diff || array_key_exists($frame_field_name_flat, $item)) {
															$formatted_item[$field_name][$frame-1][$frame_field_name] = isset($item[$frame_field_name_flat])?self::_number($item[$frame_field_name_flat]):0;
														}
													}
													break;
												case 'boolean':
													if ($forDB) {
														if (!$diff || array_key_exists($frame_field_name, $item[$field_name][$frame-1])) {
															$formatted_item[$frame_field_name_flat] = isset($item[$field_name][$frame-1][$frame_field_name])?self::_boolean($item[$field_name][$frame-1][$frame_field_name]):null;
														}
													} else {
														if (!$diff || array_key_exists($frame_field_name_flat, $item)) {
															$formatted_item[$field_name][$frame-1][$frame_field_name] = isset($item[$frame_field_name_flat])?self::_boolean($item[$frame_field_name_flat]):null;
														}
													}
													break;
												default:
													if ($forDB) {
														if (!$diff || array_key_exists($frame_field_name, $item[$field_name][$frame-1])) {
															$formatted_item[$frame_field_name_flat] = isset($item[$field_name][$frame-1][$frame_field_name])?self::_string($item[$field_name][$frame-1][$frame_field_name]):"";
														}
													} else {
														if (!$diff || array_key_exists($frame_field_name_flat, $item)) {
															$formatted_item[$field_name][$frame-1][$frame_field_name] = isset($item[$frame_field_name_flat])?self::_string($item[$frame_field_name_flat]):"";
														}
													}
											}
										}
									}
								}
							}
							break;
						default:
							if (!$diff || array_key_exists($field_name, $item)) {
								$formatted_item[$field_name] = isset($item[$field_name])?self::_string($item[$field_name]):"";
							}
					}
				}
			}
			return count($formatted_item)>0?$formatted_item:false;
		}
		return false;
	}
}
