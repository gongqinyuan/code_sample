<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::post('register', 'Auth\RegisterController@register')->middleware(['ipcheck','web','guest']);
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware(['ipcheck','web','guest']);


// this huge route with a lot of parameters is just for Vue.JS routes
Route::get('/admin/{section?}/{id?}/{subid?}/{itemid?}/{itemversion?}', 'AdminController@index')->name('admin');

Route::group(['middleware' => ['auth', 'bannerbot'], "prefix" => "api"], function () {
	
	
	
	Route::resource('overview', 'API\OverviewController', ['only' => ['show', 'update']]);
	Route::resource('download', 'API\DownloadController', ['only' => ['index']]);
	Route::resource('upload',   'API\UploadController',   ['only' => ['store']]);
	
	   Route::get('acl',                          'API\ACLController@index'              )->name('acl.index');
	   Route::get('bannersizes',                  'API\BannerSizesController@index'      )->name('bannersizes.index');
	   Route::get('bannersSettings',              'API\BannersSettingsController@index'  )->name('bannersettings.index');
	   Route::get('overviewSettings',             'API\OverviewSettingsController@index' )->name('overviewsettings.index');
	
	   Route::get('category',                     'API\CategoriesController@index'       )->name('category.index');
	   Route::get('category/{category}/{size}',   'API\CategoriesController@show'        )->name('category.show');
	
	   Route::get('banner/{size}/{ID}',           'API\BannerController@versions'        )->name('banner.version');	// get banner versions
	   Route::get('banner/{size}/{ID}/{version}', 'API\BannerController@show'            )->name('banner.show');	// get banner data
	  Route::post('banner/{size}',                'API\BannerController@store'           )->name('banner.store');	// create new banner
	   Route::put('banner/{size}/{ID}',           'API\BannerController@update'          )->name('banner.update');	// save current banner
	Route::delete('banner/{size}/{ID}',           'API\BannerController@destroy'         )->name('banner.destroy');	// unpublish and remove banner
	
	  Route::post('publish/{size}/{ID}',          'API\PublishController@store'          )->name('pubish.store');	// publish banner
	Route::delete('publish/{size}/{ID}',          'API\PublishController@destroy'        )->name('pubish.destroy');	// unpublish banner
});
