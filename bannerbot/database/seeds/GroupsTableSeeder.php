<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$data = [
			[
				"name"      => "Admin", 
				"init"      => "/admin/overview/", 
				"is_active" => true, 
				"access" => [
					[ "access" => "/admin/items/",    "r" => false, "w" => false, "x" => true  ],
					[ "access" => "/admin/overview/", "r" => false, "w" => false, "x" => true  ],
					[ "access" => "banners",          "r" => true , "w" => true , "x" => true  ],
					[ "access" => "bannersizes",      "r" => true , "w" => false, "x" => false ],
					[ "access" => "cats",             "r" => true , "w" => false, "x" => false ],
					[ "access" => "deleteBanners",    "r" => true , "w" => true , "x" => false ],
					[ "access" => "overview",         "r" => true , "w" => true , "x" => true  ],
					[ "access" => "uploadImage",      "r" => true , "w" => true , "x" => false ],
					[ "access" => "_overviewbanner",  "r" => false, "w" => false, "x" => true  ]
				]
			],
			[
				"name"      => "Media", 
				"init"      => "/admin/overview/", 
				"is_active" => true, 
				"access" => [
					[ "access" => "/admin/items/",    "r" => false, "w" => false, "x" => false ],
					[ "access" => "/admin/overview/", "r" => false, "w" => false, "x" => true  ],
					[ "access" => "banners",          "r" => true , "w" => false, "x" => false ],
					[ "access" => "bannersizes",      "r" => true , "w" => false, "x" => false ],
					[ "access" => "cats",             "r" => true , "w" => false, "x" => false ],
					[ "access" => "deleteBanners",    "r" => false, "w" => false, "x" => false ],
					[ "access" => "overview",         "r" => true , "w" => true , "x" => false ],
					[ "access" => "uploadImage",      "r" => false, "w" => false, "x" => false ],
					[ "access" => "_overviewbanner",  "r" => false, "w" => false, "x" => false ]
				]
			],
			[
				"name"      => "Client", 
				"init"      => "/admin/overview/", 
				"is_active" => true, 
				"access" => [
					[ "access" => "/admin/items/",    "r" => false, "w" => false, "x" => true  ],
					[ "access" => "/admin/overview/", "r" => false, "w" => false, "x" => true  ],
					[ "access" => "banners",          "r" => true , "w" => true , "x" => true  ],
					[ "access" => "bannersizes",      "r" => true , "w" => false, "x" => false ],
					[ "access" => "cats",             "r" => true , "w" => false, "x" => false ],
					[ "access" => "deleteBanners",    "r" => true , "w" => true , "x" => false ],
					[ "access" => "overview",         "r" => true , "w" => true , "x" => true  ],
					[ "access" => "uploadImage",      "r" => true , "w" => true , "x" => false ],
					[ "access" => "_overviewbanner",  "r" => false, "w" => false, "x" => true  ]
				]
			]
		];
		DB::transaction(function () use (&$data) {
			foreach ($data as $value) {
				$group_id = DB::table('groups')->select('id')->where('name',$value['name'])->first();
				if ($group_id === null) {
					$group_id = DB::table('groups')->insertGetId([
						'name'      => $value['name'],
						'init'      => $value['init'],
						'is_active' => $value['is_active'],
					]);
				} else {
					$group_id = $group_id->id;
					DB::table('groups')->where('id', $group_id)->update([
						'name'      => $value['name'],
						'init'      => $value['init'],
						'is_active' => $value['is_active'],
					]);
				}
				foreach ($value['access'] as $value_access) {
					$item_id = DB::table('groups_accesses')->select('id')->where('group_id', $group_id)->where('access',$value_access['access'])->first();
					if ($item_id === null) {
						$item_id = DB::table('groups_accesses')->insertGetId([
							'group_id' => $group_id,
							'access'   => $value_access['access'],
							'r'        => $value_access['r'],
							'w'        => $value_access['w'],
							'x'        => $value_access['x']
						]);
					} else {
						$item_id = $item_id->id;
						DB::table('groups_accesses')->where('id', $item_id)->update([
							'group_id' => $group_id,
							'access'   => $value_access['access'],
							'r'        => $value_access['r'],
							'w'        => $value_access['w'],
							'x'        => $value_access['x']
						]);
					}
				}
			}
		});
    }
}
