<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$data = [
			"bannersizes" => [ 
				"type" => "json", 
				"hidden" => 0, 
				"value" => ""
			],
			"bannersSettings" => [ 
				"type" => "json", 
				"hidden" => 0, 
				"value" => "" 
			],
			"exportSettings" => [ 
				"type" => "json", 
				"hidden" => 0, 
				"value" => "" 
			],
			"cats" => [ 
				"type" => "json", 
				"hidden" => 0, 
				"value" => "" 
			],
			"overviewSettings" => [ 
				"type" => "json", 
				"hidden" => 0, 
				"value" => "" 
			],
			"vendorSettings" => [ 
				"type" => "json", 
				"hidden" => 0, 
				"value" => "" 
			],
			"lastSync" => [ 
				"type" => "json", 
				"hidden" => 1, 
				"value" => '{"dt":"0000-00-00 00:00:00"}' 
			]
		];
		
		DB::transaction(function () use (&$data) {
			foreach ($data as $key => $value) {
				if (DB::table('settings')->select("k")->where("k", $key)->first() === null) {
					// add item only if it is not exists
					DB::table('settings')->insert([
						"k"      => $key,
						"v"      => $value["value"],
						"vtype"  => $value["type"],
						"hidden" => $value["hidden"]
					]);
				}
			}
		});
    }
}
