<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyncTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sync_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('bannerSize', 32);										// tab in Google docs, e.g. "300x250"
            $table->enum('progress', 
                ['wait', 'run', 'finished', 'finished_by_parent'])
                ->default('wait');											// task progress status
            $table->boolean('success')->default(false);									// result of the task (when progress is "finished")
            $table->smallInteger('retry')->default(3);									// retry on fail
            $table->text('result');											// JSON with task result (success of failed)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sync_tasks');
    }
}
